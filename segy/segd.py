import os
from constants import header_struc, format_code, mapping_header, channel_set_type, record_type
from pack_unpack import unpack
from segd_to_segy import ebcdic, binary, EBCDIC_HEADER_LENGTH, th, trace_convert
import copy
from utils import walk_files

# from progressbar import ProgressBar, Bar, Percentage, ETA


EXT_SEGY = '.sgy'
EBCDIC_HEADER_SIZE = 3200
BINARY_HEADER_SIZE = 400
TH_SIZE = 240


# TODO: review unpack
# TODO: add docstring, doctest
# TODO: unittest
# TODO: handling exceptions, try/except, errors
# TODO: logging
# TODO: command line


class Segd:
    def __init__(self, filename=None, bytestream=None, inspect_nb_samples=True, container=None, ffs=1):
        # type: (object, object, object, object, object) -> object
        self.bytestream = bytestream
        self.filename = self._get_filename(filename)
        self.container = container
        self.ffs = ffs
        self.open_segd = self.open(filename, bytestream)
        self.size = self._get_size(filename, bytestream)
        self.gh1 = self._set_gh1()
        # Test if file or bytestream is SEG-D (first check) checking format code
        if not self._test_gh1(self.gh1):
            return
        self.gh2 = self._set_gh2()
        self.general_header_blocks = self._get_general_header_blocks()
        self.ghn = self._set_ghn()
        self.scan_type_per_record = int(self._get_header_value("scan_type_per_record"))
        self.channel_set_per_scan_type = self._get_channel_set_per_scan_type()
        self.skew_blocks = int(self._get_header_value("skew_blocks"))
        self.extended_header_blocks = self._get_extended_header_blocks()
        self.external_header_blocks = self._get_external_header_blocks()
        self.extra_header_blocks = self._get_extra_header_blocks()
        self.extended_header = self._set_extended_header()
        self.external_header = self._set_external_header()
        self.extra_header = self._set_extra_header()
        self.block_header_size = self._get_block_header_size()
        self.ff = self._get_ff()
        self.sp = self._get_sp()
        self.line = self._get_line()
        self.sample_rate = self._get_sample_rate()
        self.channel_set, self.channel_set_range = self._set_channel_set()
        self.traces_per_scan_type = self._get_traces_per_scan_type()
        self.trace_map = {1: {"offset": self.block_header_size}}
        self.total_traces = self.get_total_traces()
        if inspect_nb_samples:  # To work with only block header
            self._adjust_nb_samples()

    def info(self):
        """Print common info of the Segd object"""
        buf = "\nCommon Information\n\n"
        buf += "File Name            : %s\n\n" % (self.filename if self.filename else 'Bytestream')
        buf += "File Number (FFID)   : %i\n" % self.ff
        buf += "SP                   : %s\n" % (
            self._get_header_value("source_point_number", self.general_header_blocks)
            if self.general_header_blocks > 1
            else 'N/A')
        buf += "Data Format          : %s - %s\n" % (self._get_header_value("format_code"),
                                                     self._get_header_human_name(
                                                         dict=format_code,
                                                         header=self._get_header_value("format_code")))
        buf += "Base Sample Interval : %i ms\n" % self.sample_rate
        buf += "Total Traces #       : %i\n\n" % self.total_traces
        buf += "Channel Sets (%i):\n\n" % self.channel_set_per_scan_type
        buf += "%-5s  %-22s%-8s%-11s%-8s%-11s\n" % ('#', 'type', '#chan', '#samples', 't0 (ms)', 'tn (ms)')

        cs = [k for k in self.channel_set.iterkeys()]
        order_ch = sorted(cs, key=lambda ch: (ch.split('.')[0], ch.split('.')[1]))
        for ch_set in order_ch:
            buf += "%-5s  %-22s%-8s%-11i%-8i%-11i\n" % (
                ch_set,
                channel_set_type[self._get_header_value("channel_type", ch_set)]["descr"],
                int(self._get_header_value("number_of_channels", ch_set)),
                int(self._get_header_value("nb_samples", ch_set)),
                int(self._get_header_value("channel_set_start_time", ch_set)),
                int(self._get_header_value("channel_set_end_time", ch_set)),
            )

        buf += "\n#Extended Headers Blocks : %i\n" % self.extended_header_blocks
        buf += "#External Headers Blocks : %i\n" % self.external_header_blocks
        buf += "#Extra Headers Blocks    : %i\n" % self.extra_header_blocks

        buf += "\nFile Header Length : %i\n" % self.block_header_size
        buf += "File Length        : %i\n\n" % self.size
        print buf
        return buf

    def info_gh1(self):
        """Print info gh1 block header"""
        buf = "\nGeneral Header Block #1\n\n"
        # Order dictionary with pos to display teh headers in a good order
        header_pos = [(i, k["pos"]) for i, k in self.gh1.iteritems() if i != "offset"]
        ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
        buf += ''.join(
            ["%-32s %-4s : %-20s\n" % (self._get_header_human_name(header[0]), self._get_header_code(header[0]),
                                       self._get_header_value(header[0])) for header in ordered_keys
             ]
        )
        print buf
        return buf

    def info_gh2(self):
        """Print info gh2 block header"""
        buf = "\nGeneral Header Block #2\n\n"
        # Order dictionary with pos to display the headers in a good order
        header_pos = [(i, k["pos"]) for i, k in self.gh2.iteritems() if i != "offset"]
        ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
        buf += ''.join(
            ["%-32s %-4s : %-20s\n" % (self._get_header_human_name(header[0]), self._get_header_code(header[0]),
                                       self._get_header_value(header[0])) for header in ordered_keys
             ]
        )
        print buf
        return buf

    def info_ghn(self):
        """Print info of gh3 to ghn blocks header"""
        for gh, headers in self.ghn.iteritems():
            buf = "\nGeneral Header Block #%i\n\n" % gh
            header_pos = [(i, k["pos"]) for i, k in headers.iteritems() if i != "offset"]
            # TODO: order general header dict (not only headers dict)
            ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
            buf += ''.join(
                ["%-32s %-4s : %-20s\n" % (
                    self._get_header_human_name(header[0], gh), self._get_header_code(header[0], gh),
                    self._get_header_value(header[0], gh)
                ) for header in ordered_keys
                 ]
            )
            print buf
            return buf

    def info_channel_set(self):
        """Print info of channel set block header"""
        cs = [k for k in self.channel_set.iterkeys()]
        order_ch = sorted(cs, key=lambda ch: (ch.split('.')[0], ch.split('.')[1]))
        # order self.channel_set keys (to display in ascending key)
        for ch_set in order_ch:
            buf = "\nChannel Set #%s\n\n" % ch_set
            header_pos = [(i, k["pos"]) for i, k in self.channel_set[ch_set].iteritems()
                          if i != "offset" and i != "chset" and i != "nb_samples"]
            ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
            buf += ''.join(
                ["%-32s %-4s : %-20s\n" % (
                    self._get_header_human_name(header[0], ch_set), self._get_header_code(header[0], ch_set),
                    self._get_header_value(header[0], ch_set)
                ) for header in ordered_keys
                 ]
            )
            print buf
            return buf

    def read_block_header(self, block_name, block_number=None):
        """
        Reads block header

        Args:
            block_name (str): Name of the block header. Must be comprised in: gh1, gh2, ghn, channel_set,
            extended_header, external_header, extra_header, trace_header, trace_header_extension

            block_number (optional): In case of block_name in (ghn, channel_set) a block_number must be precised. In
            case og ghn it's an int and a str index (eg. '1.16') in the case of channel_set

        Returns:
            str: bytestream of the block_header
        """
        pos = self._get_offset_block_header(block_name, block_number)
        if block_name in ["extended_header", "external_header", "extra_header"]:
            length = getattr(self, block_name + '_blocks') * BLOCK_SIZE
        else:
            length = BLOCK_SIZE
        return self._read(pos, length)

    def dump_block_header(self, block_name, block_number=None, length=16):
        """
        Represents the bytestream of block_header in a human way.
        The parameters are passed to method read_block_header method and the output of this function is passed to
        hexdump

        Args:
            block_name (str): Name of the block header. Must be comprised in: gh1, gh2, ghn, channel_set,
            extended_header, external_header, extra_header, trace_header, trace_header_extension

            block_number (optional): In case of block_name in (ghn, channel_set) a block_number must be precised. In
            case of ghn it's an int and a str index (eg. '1.16') in the case of channel_set

        Returns:
            str to standard output: Produced by hexdump function
        """
        block_header = self.read_block_header(block_name, block_number)
        print hexdump(block_header, length=length)

    @staticmethod
    def _get_range(type):
        """Returns the length of data defined by a type

        Args:
            type(str): string representation of the word's codification

        Returns:
            int: represents number of bytes
        """
        rango = None
        if type[0] in ['H', 'n', 'N', 'B', 'S', 'E', 's'] and (type[1] == '+' or type[1] == "-"):
            rango = int(float(type[2:])) + 1
        elif type[0] in ['H', 'n', 'N', 'B', 'S', 'E', 's'] and int(type[1]) in range(9):
            rango = int(type[1:])
        return rango

    def _get_offset_block_header(self, blockname, block_number=None):
        """
        Returns the offset of the block header. Offset is obtained through a method in function of the block_name
        (eg. _get_extended_header_offset())

        Args:
            block_name (str): Name of the block header. Must be comprised in: gh1, gh2, ghn, channel_set,
            extended_header, external_header, extra_header, trace_header, trace_header_extension

            block_number (optional): In case of block_name in (ghn, channel_set) a block_number must be precised. In
            case of ghn it's an int and a str index (eg. '1.16') in the case of channel_set

        Returns:
            int: offset of the block header
        """
        if blockname.startswith('gh'):
            offset_block = self._get_gh_offset(blockname, block_number)
        elif blockname.startswith('ch'):
            offset_block = self._get_channel_set_offset(block_number)
        elif blockname.startswith('extended'):
            offset_block = self._get_extended_header_offset()
        elif blockname.startswith('external'):
            offset_block = self._get_external_header_offset()
        elif blockname.startswith('extra'):
            offset_block = self._get_extra_header_offset()
        return offset_block

    def _get_pos_range_type_header(self, header, block_number=None, pos=None, length=None):
        """Returns the absolute offset of the header, the range of bytes to read and the type of the header.
        Called by the _read_header_value method.

        Args:
            header (str): Name of the header. This name must be part of the headers set in the segd dictionnary (see
            constants.py) or one of the created header (see add_header method)

            block_number (optional): In case of the header is part of block headers ghn or channel_set) a
            block_number must be precised. In case og ghn it's an int and a str index (eg. '1.16') in the case of
            channel_set

            pos (optional, int): In case of the trace header the position has already been set through index_trace
            method and stored in self.trace_map object

            length (optional, int): Not used at the moment, but keep the possibility to manually attribute a value to
            the length (in bytes)

        Returns:
            pos (int): Absolute position of the header
            length (int): Number of bytes to read to get the header value
            type (str): type of the header read in the segd format dictionnary (see constants.py)
        """
        blockname = mapping_header[header]
        # Define pos
        if not pos:  # Case block_header
            offset_block = self._get_offset_block_header(blockname, block_number)
            pos = offset_block + self._get_header_pos(header, dict=header_struc[blockname])
        elif pos and blockname in ["trace_header", "trace_header_extension"]:
            pos += self._get_header_pos(header, dict=header_struc[blockname])
        # Define type
        type = self._get_header_type(header, dict=header_struc[blockname])
        # Define rango
        if not length:
            length = self._get_range(type)
        return int(pos), int(length), type

    def _read_header_value(self, header, block_number=None, pos=None, length=None):
        """Read the header defined by position, length and type into the file.
        In function of the mandatory attributes to read a header (absolute offset, range of bytes to read and type)
        passed to the method the method completes them through _get_pos_range_type_header method and/or
        _get_header_type method.
        The raw data is then sent to the unpack method to return the value in function of the type.

        Args:
            header (str): Name of the header. This name must be part of the headers set in the segd dictionnary (see
            constants.py) or one of the created header (see add_header method)

            block_number (optional): In case of the header is part of block headers ghn or channel_set) a
            block_number must be precised. In case og ghn it's an int and a str index (eg. '1.16') in the case of
            channel_set

            pos (optional, int): In case of the trace header the position has already been set through index_trace
            method and stored in self.trace_map object

            length (optional, int): Not used at the moment, but keep the possibility to manually attribute a value to
            the length (in bytes)

        Returns:
            Value of the header (int, float, str)
        """
        if not pos or not length:
            pos, length, type = self._get_pos_range_type_header(header, block_number, pos, length)
        data = self._read(pos, length)
        if not type:
            blockname = mapping_header[header]
            type = self._get_header_type(header, dict=header_struc[blockname])
        return unpack(data, type)

    def _set_gh1(self):
        """Init method that returns the gh1 dictionnary.
        Copy the segd gh1 dictionnary (see constants. py) (headers, and for each header: code, pos, type, human name)
        and for each header attributes the value read into the file/bytestream through the _read_header_value method

        Returns:
            dict: gh1 dictionnary with read values
        """
        gh1 = copy.deepcopy(header_struc["gh1"])
        for header in gh1.iterkeys():
            if header != "offset":
                gh1[header]["value"] = self._read_header_value(header)
        return gh1

    def _test_gh1(self, gh1):
        """Method that checks integrity of the object checking format code (discriminative first integrity check).
        Exit script if condition not fulfilled

        Args:
            gh1: General Header #1 (dict)

        Returns:
            bool: False =  Not SEG-D
        """
        if gh1["format_code"]["value"] not in [k for k in format_code.iterkeys()]:
            print "File %s not recognized" % self._get_file_name_print()
            return False
        else:
            return True

    def _set_gh2(self):
        """Init method that returns the gh2 dictionnary.
        Copy the segd gh2 dictionnary (see constants. py) (headers, and for each header: code, pos, type, human name)
        and for each header attributes the value read into the file/bytestream through the _read_header_value method

        Returns:
            dict: gh2 dictionnary with read values"""
        if self._get_header_value("blocks_in_general_header") > 0:
            gh2 = copy.deepcopy(header_struc["gh2"])
            for header in gh2.iterkeys():
                if header != "offset":
                    gh2[header]["value"] = self._read_header_value(header)
        else:
            gh2 = None
        return gh2

    def _set_ghn(self):
        """
        Init method that returns the ghn dictionnary (dictionary of dictionary,
        where key dict1 = General header block).
        For each ghn block, copy the segd ghn dictionnary (see constants. py) (headers, and for each header: code, pos,
        type, human name)
        and for each header attributes the value read into the file/bytestream through the _read_header_value method

        Returns:
            dict: ghn dictionnary with read values
        """
        if self.general_header_blocks > 2:
            ghn = {}
            for i in range(3, self.general_header_blocks + 1):
                ghn[i] = copy.deepcopy(header_struc["ghn"])
                for header in ghn[i].iterkeys():
                    if header != "offset":
                        ghn[i][header]["value"] = self._read_header_value(header, i)
        else:
            ghn = None
        return ghn

    def _set_scan_type(self):
        """Returns scan type dictionary (dictionary of dictionary,
        where key dict1 = scan_type nb and key nested key = channel_set nb

        Not used at the moment (the scan type number is comprised into the channel_set dictionnary keys)
        """
        scan_type = {}
        for scan_type_nb in range(1, self.scan_type_per_record + 1):
            scan_type[scan_type_nb] = self._set_channel_set()

    def _set_channel_set(self):
        """Returns channel_set dictionary (dictionary of dictionary,
        where key dict1 = 'scan_type.channel_set_number')) and channel_set range dictionnary"""
        ch_set = {}
        lead0_range = len(str(self.channel_set_per_scan_type + 1))
        for i in range(1, self.scan_type_per_record + 1):
            for j in range(1, self.channel_set_per_scan_type + 1):
                index = "%s.%s" % (
                    i, str(j).zfill(lead0_range))  # We construct the keys of the channel set dict reflecting the
                # scan_type value. Eg. for the 3rd channel set of the 2nd scan type the key will be 2.3
                ch_set[index] = copy.deepcopy(header_struc["channel_set"])
                for header in ch_set[index].iterkeys():
                    if header != "offset" and header != "nb_samples":
                        ch_set[index][header]["value"] = self._read_header_value(header, index)
                ch_set[index]["chset"] = j  # Header add in dictionnary to help when looking for a particular ch_set
            # From the raw value of the channel set we calc the time values and add a nb_samples key:
            for index in ch_set.iterkeys():
                start_time = self._get_time(self._get_header_value("channel_set_start_time", index, ch_set))
                ch_set[index]["channel_set_start_time"]["value"] = start_time

                end_time = self._get_time(self._get_header_value("channel_set_end_time", index, ch_set))
                ch_set[index]["channel_set_end_time"]["value"] = end_time

                nb_samples = self._get_nb_samples(start_time, end_time,
                                                  self._get_header_value("samples_exponent", index,
                                                                         ch_set))
                ch_set[index]["nb_samples"] = {"value": int(nb_samples)}

        ch_set_range = {}  # Dictionnary that maps the limit of tr_nb inside each channel set. Used to extract ch_set,
        # scan type from the trace, tr_nb in ch_set from the abs tr_nb
        if self.scan_type_per_record > 0:
            prev_tr_nb = 1
            for i in range(1, self.channel_set_per_scan_type + 1):
                index = "%s.%s" % (str(1), str(i).zfill(lead0_range))
                if int(self._get_header_value("number_of_channels", index, ch_set)) > 0:
                    last_tr_nb = prev_tr_nb + int(self._get_header_value("number_of_channels", index, ch_set)) - 1
                    ch_set_range[i] = (prev_tr_nb, last_tr_nb, index)
                    prev_tr_nb = last_tr_nb + 1

        return ch_set, ch_set_range

    def _set_extended_header(self):
        if self.extended_header_blocks:
            return copy.deepcopy(header_struc["extended_header"])
        else:
            return None

    def _set_external_header(self):
        if self.external_header_blocks:
            return copy.deepcopy(header_struc["external_header"])
        else:
            return None

    def _set_extra_header(self):
        if self.extra_header_blocks:
            return copy.deepcopy(header_struc["extra_header"])
        else:
            return None

    @staticmethod
    def _get_size(filename, bytestream):
        """
        Returns size of the Segd object in function of the input (bytestream or file)

        Returns:
            int: size of the SEG-D object
        """
        if bytestream:
            return len(bytestream)
        else:
            return os.path.getsize(filename)

    @staticmethod
    def _get_filename(filename):
        """
        Returns the filename of the object if exists

        Returns:
            str: filename of the object
        """
        file_name = None
        if filename:
            file_name = filename
        return file_name

    # TODO: check if file already opened first
    @staticmethod
    def open(filename, bytestream=None):
        """
        Returns open file object if the input of the class is a file,
        else returns None

        Args:
            filename (str): Filename of the file to open
            bytestream (bool): True if input of the class is a byte stream

        Returns:
            Open file object
        """
        if bytestream:
            return None
        else:
            return open(filename, 'r')

    def close(self):
        if self.filename:
            self.open_segd.close()
            return
        else:
            return None

    def _get_general_header_blocks(self):
        return int(self._get_header_value("blocks_in_general_header")) + 1

    def _get_block_header_size(self):
        return (self.general_header_blocks + self.scan_type_per_record * (self.channel_set_per_scan_type +
                                                                          self.skew_blocks) +
                self.extended_header_blocks + self.external_header_blocks + self.extra_header_blocks) * BLOCK_SIZE

    def _get_channel_set_per_scan_type(self):
        channel_set_per_scan_type = self._get_header_value("channel_set_per_scan_type")
        if channel_set_per_scan_type == "ff":
            channel_set_per_scan_type = self._get_header_value("expanded_channel_set_per_scan_type")
        return int(channel_set_per_scan_type)

    def _get_extended_header_blocks(self):
        extended_header_blocks = self._get_header_value("extended_header_block")
        if extended_header_blocks == "ff":
            extended_header_blocks = self._get_header_value("expanded_extended_header_block")
        return int(extended_header_blocks)

    def _get_external_header_blocks(self):
        external_header_blocks = self._get_header_value("external_header_block")
        if external_header_blocks == "ff":
            external_header_blocks = self._get_header_value("expanded_external_header_block")
        return int(external_header_blocks)

    def _get_extra_header_blocks(self):
        try:
            if int(self._get_header_value("manufacturer_code")) == 34 and self.extended_header_blocks > 0:
                # TODO: reprendre bonnes valeurs
                return int(self._get_header_value("extra_header_block"))
            else:
                return 0
        except ValueError:
            return 0

    def _get_ff(self):
        ff = self._get_header_value("ff")
        if ff == "ffff":
            ff = self._get_header_value("expanded_ff")
        return int(ff)

    def _get_sp(self):
        if self.general_header_blocks > 1:
            return int(self._get_header_value("source_point_number", self.general_header_blocks))
        else:
            return None

    def _get_line(self):
        if self.general_header_blocks > 1:
            return int(self._get_header_value("source_line_number", self.general_header_blocks))
        else:
            return None

    def _get_sample_rate(self):
        base_scan_interval = self._get_header_value("base_scan_interval")
        a = 3
        sample_rate = 0
        for bit in base_scan_interval:
            sample_rate += int(bit) * (2 ** a)
            a -= 1
        return sample_rate

    @staticmethod
    def _get_time(time_binary):
        # TODO: Review and simplify formula (should be simple struct.unpack)
        """
        Method called by _set_channel_set method to return time in ms in function of the binary value of the time

        Args:
            time_binary (bit string): Starting time word read in channel set

        Returns:
            Int: Time in milliseconds
        """
        a = 16
        time = 0
        for bit in time_binary:
            time += int(bit) * (2 ** a)
            a -= 1
        return int(time)

    def add_header(self, name=None, block_header=None, block_number=None, rel_offset=0, type=None, strip=False):
        if getattr(self, block_header) is None:
            print "Block %s doesn't exist" % block_header
            return
        range = self._get_range(type)
        # TODO: Add check if rel_offset + range > block_header_size -> Out of Bounds
        # if rel_offset + range >

        new_header = {
            "code": "XX",
            "pos": rel_offset,
            "type": type,
            "human_name": name
        }

        header_struc[block_header][name] = new_header
        mapping_header[name] = block_header
        if block_number:
            getattr(self, block_header)[block_number][name] = new_header
            if strip:
                getattr(self, block_header)[block_number][name]["value"] = self._read_header_value(name,
                                                                                                   block_number).strip()
            else:
                getattr(self, block_header)[block_number][name]["value"] = self._read_header_value(name, block_number)
        else:
            getattr(self, block_header)[name] = new_header
            if strip:
                getattr(self, block_header)[name]["value"] = self._read_header_value(name).strip()
            else:
                getattr(self, block_header)[name]["value"] = self._read_header_value(name)

    def qc(self, limit0=1, limit1=None):
        """
        Method that realizes the qc of trace header (see test_tr_header method) on a given range of traces or on all
        the traces (limit1=None)

        Args:
            limit0 (int): First trace to qc
            limit1: Last trace to qc

        Returns:
            break if error (+ test_tr_header error message)
            qc passed message if no error

        """
        if not limit1:
            limit1 = self.total_traces
        for tr_nb in range(limit0, limit1 + 1):
            res = self.test_tr_header(tr_nb)
            if not res:  # Case test_tr_header gives error (string returned)
                break
        print "File: %s - traces range: %s --> ok" % (self._get_file_name_print(),
                                                      "all" if limit0 == 1 and limit1 == self.total_traces
                                                      else '(' + str(limit0) + ',' + str(limit1) + ')')

    def _get_nb_samples(self, time1, time2, sample_exponent):
        """
        Called by _set_channel_set to return the nb samples of a channel set in function of the start time (time1),
        the end time (time2), and the sample exponent

        Args:
            time1 (int): Starting time of the channel set
        """
        nb_samples = ((time2 - time1) / self.sample_rate) * sample_exponent
        return nb_samples

    def _adjust_nb_samples(self):
        """
        Function made to face with the 'Sercel' byte issue. It tests the abs tr_nb #2 with the values of nb_samples and
        nb_samples +1 read in the channel set header (from start time, end time, samples exponent)
        We assume  thatall channel sets share the same nb samples pattern
        """
        for index, chset in self.channel_set.iteritems():  # We first test the nb_samples of the first channel set
            if chset["chset"] == 1:
                nb_samples = self._get_header_value("nb_samples", index)
                break
        # Case it doesn't work with nb samples standard (ie nb samples first channel set) (test_tr_header returns False)
        # If it works with nb samples standard this condition is never reached and the function stops with no effect
        if not self.test_tr_header(2, nb_samples):
            # self.trace_hash.pop(2)  # Reset values
            self.trace_map = {1: {"offset": self.block_header_size}}
            # We test with nb samples + 1 -  If it works (test_tr_header returns True) we update
            # the channel set dictionary with updated nb samples value
            if self.test_tr_header(2, nb_samples + 1):
                for chset in self.channel_set.itervalues():
                    if int(chset["number_of_channels"]["value"]) > 0:
                        chset["nb_samples"]["value"] += 1
            else:
                print "File %s corrupted" % self._get_file_name_print()

    def test_tr_header(self, tr_nb, nb_samples=None):
        """Compare the value of the file number, scan type number, channel set number and trace number in the trace
        header with these values expected from the file structure (from block header)
        Authorizes some tolerance for the trace number according to the variations that can exist in the SEG-D format

        Args:
            tr_nb (int): Trace number within the SEG-D file
            nb_samples (int): Used in the case of the _adjust_nb_samples method to read the tested trace header to an
            offset given by the tested nb_samples

        Returns:
            bool: Result of the test

        """
        scan_type, ch_set, tr_nb_in_chset = self._get_tr_nb_index(tr_nb)
        if self._get_th_value(tr_nb, "tr_file_number", nb_samples) != self._get_header_value("ff") or \
                        int(self._get_th_value(tr_nb, "tr_scan_type_number", nb_samples)) != scan_type or \
                        int(self._get_th_value(tr_nb, "tr_channel_set_number", nb_samples)) != ch_set:
            if not nb_samples:  # Means not in case of _adjust_nb_samples where test can possibly return false
                print "File %s corrupted tr# %i (scan type: %i, chset: %i)" % (self._get_file_name_print(),
                                                                               tr_nb, scan_type, ch_set)
            return False
        # Case trace number into channel set that can vary (=sum traces, or tr_nb in chset, etc...)
        elif int(self._get_th_value(tr_nb, "trace_number", nb_samples)) == tr_nb or \
                        int(self._get_th_value(tr_nb, "trace_number", nb_samples)) == tr_nb_in_chset:
            return True
        # Test not adapted to _adjust_nb_samples that uses only 2 traces and this test uses the total amount of traces
        elif not nb_samples \
                and int(self._get_th_value(tr_nb, "trace_number", nb_samples)) in range(1, self.total_traces + 1):
            print "File %s: Review trace pattern for: trace#: %i, scan type: %i, chset: %i" % \
                  (self._get_file_name_print(), tr_nb, scan_type, ch_set)
        else:
            if not nb_samples:  # Means not in case of _adjust_nb_samples where test can possibly return false
                print "File %s corrupted tr# %i (scan type: %i, chset: %i)" % (self._get_file_name_print(),
                                                                               tr_nb, scan_type, ch_set)
            return False

    def _get_th_value(self, tr_nb, header, nb_samples=None):
        """
        Return trace header value.
        If trace header already read, directly read from trace_map
        elif trace already mapped but header not read, header read and trace_map updated
        elif trace not mapped, trace indexed (including all the previous ones not indexed yet) and header read
        """
        if tr_nb in self.trace_map:
            if header in self.trace_map[tr_nb]:
                return self.trace_map[tr_nb][header]
            else:
                self.trace_map[tr_nb][header] = self._read_header_value(header, pos=self.trace_map[tr_nb]["offset"])
                return self.trace_map[tr_nb][header]
        else:
            for trnb in range(max(self.trace_map), tr_nb + 1):
                self.index_trace(trnb, nb_samples)
            return self._get_th_value(tr_nb, header)

    def index_trace(self, tr_nb, nb_samples=None):
        """Read 'structural' value from the trace header (nb_samples, thex blocks) to map the trace"""
        # try:
        thex = int(self._read_header_value("trace_header_extensions", pos=self.trace_map[tr_nb]["offset"]))
        # except ValueError:
        #     print "Trace Value error"
        #     quit()
        if not nb_samples:  # nb_samples when _adjust_nb_samples, in this case the nb_samples to test is passed
            # to the function and not read in the th
            nb_samples = self._get_nb_samples_tr(tr_nb, thex)
        tr_size = self._get_size_tr(thex, nb_samples)
        self.trace_map[tr_nb]["thex"] = thex
        self.trace_map[tr_nb]["nb_samples"] = nb_samples
        self.trace_map[tr_nb]["tr_size"] = tr_size
        self.trace_map[tr_nb + 1] = {}
        self.trace_map[tr_nb + 1]["offset"] = tr_size + self.trace_map[tr_nb]["offset"]

    def _get_nb_samples_tr(self, tr_nb, thex):
        """
        Return the nb_samples of the trace. If there is a trace header extension it is read from the trace else
        it is read from the corresponding channel set dictionnary
        """
        if thex > 0:
            return int(self._read_header_value("nb_samples_per_trace", pos=self.trace_map[tr_nb]["offset"]))
        else:
            chset_nb = self._get_tr_nb_index(tr_nb)[1]
            for index, chset in self.channel_set.iteritems():
                # z = 3
                if chset["chset"] == chset_nb:
                    return self._get_header_value("nb_samples", index)

    def _get_size_tr(self, thex, nb_samples):
        """Return the size of the trace after have indexed it"""
        th_size = TH_SIZE + (BLOCK_SIZE * thex)
        data_size = nb_samples * format_code[self._get_header_value("format_code")]["nb_bytes"]
        return int(th_size + data_size)

    def _get_tr_nb_index(self, tr_nb):
        """
        From tr_nb returns positioning of the tr within scan type and channel sets
        in functions of the number of scan type per record, number of channels sets per scan type and channels per
        channels set described in the block header. These values will be used to test the trace header
        """
        if tr_nb % self.traces_per_scan_type == 0:
            scan_type = (tr_nb / self.traces_per_scan_type)
        else:
            scan_type = (tr_nb / self.traces_per_scan_type) + 1
        tr_nb_in_st = tr_nb - ((scan_type - 1) * self.traces_per_scan_type)
        chset_nb = 0
        tr_nb_in_chset = 0
        for ch_set_nb, limits in self.channel_set_range.iteritems():
            if limits[0] <= tr_nb_in_st <= limits[1]:
                chset_nb = ch_set_nb
                tr_nb_in_chset = self._get_tr_nb_in_chset(tr_nb_in_st, ch_set_nb)
                break

        return scan_type, chset_nb, tr_nb_in_chset

    def _get_tr_nb_in_chset(self, tr_nb, ch_set_nb):
        """
        Return tr_nb in chset from absolute tr_nb. This value will be used to test the bytes 5,6 of the trace header
        """
        sum_tr = 0
        for i in range(1, ch_set_nb):
            sum_tr += self.channel_set_range[i][1] - self.channel_set_range[i][0] + 1
        return tr_nb - sum_tr

    def _get_traces_per_scan_type(self):
        traces_per_scan_type = 0
        for ch_set in self.channel_set.iterkeys():
            if int(ch_set.split('.')[0]) == 1:
                traces_per_scan_type += int(self._get_header_value("number_of_channels", ch_set))
        return traces_per_scan_type

    def _get_header_value(self, header, block_number=None, dict=None):
        """Return the value of a header"""
        if dict:  # when the self attribute is not already defines. eg. getting time in ms when getting ch_set
            blockname = dict
        else:
            blockname = mapping_header[header]
            blockname = getattr(self, blockname)
        if block_number:
            return blockname[block_number][header]["value"]
        else:
            return blockname[header]["value"]

    def _get_header_human_name(self, header, block_number=None, dict=None):
        """Return the descr of a header"""
        if dict:  # when the self attribute is not already defines. eg. getting time in ms when getting ch_set
            blockname = dict
        else:
            blockname = mapping_header[header]
            blockname = getattr(self, blockname)
        if block_number:
            return blockname[block_number][header]["human_name"]
        else:
            return blockname[header]["human_name"]

    def _get_header_code(self, header, block_number=None, dict=None):
        """Return the descr of a header"""
        if dict:  # when the self attribute is not already defines. eg. getting time in ms when getting ch_set
            blockname = dict
        else:
            blockname = mapping_header[header]
            blockname = getattr(self, blockname)
        if block_number:
            return blockname[block_number][header]["code"]
        else:
            return blockname[header]["code"]

    def _get_header_type(self, header, block_number=None, dict=None):
        """Return the type of a header"""
        if dict:  # when the self attribute is not already defines. eg. getting time in ms when getting ch_set
            blockname = dict
        else:
            blockname = mapping_header[header]
            blockname = getattr(self, blockname)
        if block_number:
            return blockname[block_number][header]["type"]
        else:
            return blockname[header]["type"]

    def _get_header_pos(self, header, block_number=None, dict=None):
        """Return the position of a header"""
        if dict:  # when the self attribute is not already defines. eg. getting time in ms when getting ch_set
            blockname = dict
        else:
            blockname = mapping_header[header]
            blockname = getattr(self, blockname)
        if block_number:
            return blockname[block_number][header]["pos"]
        else:
            return blockname[header]["pos"]

    def print_header_value(self, header_name, block=None, dict=None):
        """Print the value of a header"""
        print self._get_header_value(header=header_name, block_number=block, dict=dict)

    @staticmethod
    def _get_gh_offset(block, block_number):
        if block_number:  # eq. if block = ghn
            return (block_number - 1) * BLOCK_SIZE
        else:
            return header_struc[block]["offset"]

    def _get_channel_set_offset(self, channel_set_nb):
        parsed_chset = channel_set_nb.split(".")
        scan_type = int(parsed_chset[0])
        channel_set_nb = int(parsed_chset[1])
        scan_type_block_size = (self.channel_set_per_scan_type + self.skew_blocks) * BLOCK_SIZE
        return BLOCK_SIZE * (self.general_header_blocks + channel_set_nb - 1) + (scan_type - 1) * scan_type_block_size

    def _get_extended_header_offset(self):
        return (self.general_header_blocks + self.scan_type_per_record *
                (self.channel_set_per_scan_type + self.skew_blocks)) * BLOCK_SIZE

    def _get_external_header_offset(self):
        return (self.general_header_blocks + self.scan_type_per_record *
                (self.channel_set_per_scan_type + self.skew_blocks) +
                self.extended_header_blocks) * BLOCK_SIZE

    def _get_extra_header_offset(self):
        return (self.general_header_blocks + self.scan_type_per_record *
                (self.channel_set_per_scan_type + self.skew_blocks) +
                self.external_header_blocks + self.extended_header_blocks + self.extra_header_blocks) * BLOCK_SIZE

    def get_total_traces(self):
        """Return the total number of traces in the Segd object

        Returns:
            int: sum(#traces per channel_set)
        """
        total_traces = 0
        for ch_set in self.channel_set.iterkeys():
            total_traces += int(self._get_header_value("number_of_channels", ch_set))
        return total_traces

    def get_aux_traces(self):
        """Returns the number of auxiliary traces in the Segd object

         Returns:
            int: sum (#traces per channel_set where channel_type not Seis)
         """
        aux_traces = 0
        for ch_set in self.channel_set.iterkeys():
            if self._get_header_value("channel_type", ch_set) != "0001":
                aux_traces += int(self._get_header_value("number_of_channels", ch_set))
        return aux_traces

    def get_nb_samples_pattern(self):
        """Method used to create the binary header with the purpose to define nb_samples pattern.
        If nb_samples varies into the different channels set the fixed length trace flag is set to zero and the
        nb_samples returned is the one of the first channel_set
        Used to create the binary header (nb_samples per trace (byte 21-22), fixed length flag (byte 302-303) during
        segd to segy conversion. If fixed length trace == 0 the nb samples per traces of the binary header will be
        set with number of samples of the first channel set

        Returns:
            int: nb_samples
            int: fixed length flag (0 or 1)
        """
        nb_samples_ref = None
        for ch_set in self.channel_set.iterkeys():
            if int(self._get_header_value("number_of_channels", ch_set)) > 0:
                if not nb_samples_ref:
                    nb_samples_ref = self._get_header_value("nb_samples", ch_set)
                elif self._get_header_value("nb_samples", ch_set) != nb_samples_ref:
                    return nb_samples_ref, 0
        return nb_samples_ref, 1

    def export_csv(self, *args, **kwargs):
        csv = ','.join(
            [str(getattr(self, header)) if hasattr(self, header) else str(self._get_header_value(header)) for header in
             args])
        if kwargs:
            with open(kwargs['output_file'], 'a+') as fout:
                fout.write(csv + "\n")
        else:
            print csv

    def convert_to_segy(self, output_file, string_ebcdic=None, ascii_textual=False, file_ebcdic=None, format_code=5,
                        resample=None, big_endian=True, sorting_code=1, segy_rev=1, extended_textual_header=0,
                        binary_header_dict=None, limit0=1, limit1=None):

        if not limit1 or limit1 > self.total_traces:
            limit1 = self.total_traces

        # Index traces to convert if not indexed yet
        if limit1 not in self.trace_map:
            for trnb in range(max(self.trace_map), limit1 + 1):
                self.index_trace(trnb)

        with open(output_file, 'wb+') as fout:
            fout.write(ebcdic(string=string_ebcdic, file_input=file_ebcdic, ascii=ascii_textual))
            fout.write(binary(self._create_binary_dict(format=format_code,
                                                       trace_sorting_code=sorting_code,
                                                       segy_rev=segy_rev,
                                                       extended_textual_header=extended_textual_header,
                                                       binary_header_dict=binary_header_dict,
                                                       limit0=limit0,
                                                       limit1=limit1
                                                       )))
            self.write_segy_traces(fout,
                                   big_endian=big_endian,
                                   resample=resample,
                                   trace_sorting_code=sorting_code,
                                   format=format_code,
                                   limit0=limit0,
                                   limit1=limit1)
            print "File %s created" % output_file

    def _create_binary_dict(self, format=5, trace_sorting_code=1, segy_rev=1, extended_textual_header=0,
                            binary_header_dict=None, limit0=1, limit1=None):
        """
        Create a 'minimum' binary dictionnary composed by the value of the mandatory fields of
        the binary header read from the object's attributes or from the convert_to_segy method's arguments.
        Possibility to overwrite 'structural' values with the binary_header_dict passed as arguments

        Returns:
            dict:
            key = Name of the header (This name must be part of the headers set in the binary_header dictionnary
            (see constants_sgy.py)
            value = raw value (will be packed in binary function)
        """
        binary_dict = dict()
        if binary_header_dict:
            for header, value in binary_header_dict.iteritems():
                binary_dict[header] = value
        if "traces_per_ensemble" not in binary_dict:
            binary_dict["traces_per_ensemble"] = limit1 - limit0 + 1
        if "aux_traces_per_ensemble" not in binary_dict:
            binary_dict["aux_traces_per_ensemble"] = self.get_aux_traces()
        if "nb_samples_per_trace" not in binary_dict or "fixed_length_flag" not in binary_dict:
            nb_samples, fixed_length_flag = self.get_nb_samples_pattern()
        if "nb_samples_per_trace" not in binary_dict:
            binary_dict["nb_samples_per_trace"] = int(nb_samples)
        if "fixed_length_flag" not in binary_dict:
            binary_dict["fixed_length_flag"] = fixed_length_flag
        if "sample_interval" not in binary_dict:
            binary_dict["sample_interval"] = int(self.sample_rate) * 1000
        if "format_code_segy" not in binary_dict:
            binary_dict["format_code_segy"] = format
        if "trace_sorting" not in binary_dict:
            binary_dict["trace_sorting"] = trace_sorting_code
        if "segy_rev" not in binary_dict:
            binary_dict["segy_rev"] = segy_rev
        if "extended_textual_header" not in binary_dict:
            binary_dict["extended_textual_header"] = extended_textual_header
        return binary_dict

    def write_segy_traces(self, segy_file, big_endian=True, resample=None, trace_sorting_code=1, format=5, limit0=1,
                          limit1=None, first_trace_nb=1):
        """
        Method that writes for each trace of the SEG-D object, the SEG-Y trace header and the SEG-Y trace data to the
        output SEG-Y file
        """
        tr_nb_file = first_trace_nb

        if limit1 not in self.trace_map:
            for trnb in range(max(self.trace_map), limit1 + 1):
                self.index_trace(trnb)

        for tr_nb in range(limit0, limit1 + 1):
            segy_file.write(th(self._create_trace_header_dictionnary(tr_nb, tr_nb_file)))
            segy_file.write(trace_convert(self._get_trace_data(tr_nb),
                                          tr_nb,
                                          self._get_header_value("format_code"),
                                          format,
                                          big_endian=big_endian))
            tr_nb_file += 1

    def _create_trace_header_dictionnary(self, tr_nb, tr_nb_file):
        scan_type, chset_nb, tr_nb_in_chset = self._get_tr_nb_index(tr_nb)
        chset_index = self.channel_set_range[chset_nb][2]
        # self.index_trace(tr_nb)

        trace_header_dict = dict()
        trace_header_dict["tr_nb_line"] = tr_nb_file
        trace_header_dict["tr_nb_segy_file"] = tr_nb
        trace_header_dict["ff"] = self.ff
        trace_header_dict["source_depth"] = tr_nb_file
        trace_header_dict["tr_nb_original_field_record"] = int(self._get_th_value(tr_nb, "trace_number"))
        if self.sp:
            trace_header_dict["sp"] = self.sp
        trace_header_dict["ensemble_number"] = chset_nb
        trace_header_dict["tr_nb_ensemble"] = tr_nb_in_chset
        trace_header_dict["tr_identification_code"] = self._get_segy_tr_id_code(chset_index)
        trace_header_dict["vertical_sum_tr"] = int(self._get_header_value("vertical_stack", chset_index))
        trace_header_dict["data_use"] = self._get_segy_data_use()
        trace_header_dict["nb_samples_tr"] = int(self.trace_map[tr_nb]["nb_samples"])
        trace_header_dict["sample_interval_tr"] = int(self.sample_rate) * 1000
        trace_header_dict["gain_type"] = int(self._get_header_value("channel_gain", chset_index))
        trace_header_dict["alias_filter_freq"] = int(self._get_header_value("alias_filter_frequency", chset_index))
        trace_header_dict["alias_filter_slope"] = int(self._get_header_value("alias_filter_slope", chset_index))
        trace_header_dict["notch_filter_freq"] = int(self._get_header_value("first_notch_filter", chset_index))
        trace_header_dict["low_cut_freq"] = int(self._get_header_value("low_cut_filter", chset_index))
        trace_header_dict["low_cut_slope"] = int(self._get_header_value("low_cut_filter_slope", chset_index))
        trace_header_dict["year"] = int(self._get_header_value("year"))
        trace_header_dict["day"] = int(self._get_header_value("day"))
        trace_header_dict["hour"] = int(self._get_header_value("hour"))
        trace_header_dict["minute"] = int(self._get_header_value("minute"))
        trace_header_dict["second"] = int(self._get_header_value("second"))
        return trace_header_dict

    def _read(self, pos, length):
        """
        Read data into segd object.
        If our object is a bytestream (defined by the object attribute filename), just extract the range of bytes
        at the good offset from the bytestream.
        elif our object is a file, opens it, seeks to the absolute offset and read the range of data.

        Args:
            pos (int): Offset of the data to read

            length (int): Length of the data to read

        Returns:
            int: bytestream of th data
        """
        if self.open_segd:
            self.open_segd.seek(pos)
            data = self.open_segd.read(length)
        else:
            data = self.bytestream[pos:pos + length]
        return data

    def close(self):
        if self.open_segd:
            self.open_segd.close()

    def _get_trace_data(self, tr_nb):
        pos, length = self._get_trace_data_offset_range(tr_nb)
        return self._read(pos, int(length))

    def _get_trace_data_offset_range(self, tr_nb):
        if tr_nb not in self.trace_map:
            for trnb in range(max(self.trace_map), tr_nb + 1):
                self.index_trace(trnb)
        th_size = TH_SIZE + (BLOCK_SIZE * self.trace_map[tr_nb]["thex"])
        pos = self.trace_map[tr_nb]["offset"] + th_size
        length = self.trace_map[tr_nb]["tr_size"] - th_size
        return pos, length

    def _get_segy_tr_id_code(self, chset_index):
        """
        Method that returns the SEG-Y trace identification code in function of the chset index.
        From the channel type of the channel set we get the correspondent SEG-Y trace identification code in the
        channel set type dictionnary (see constants.py)

        If the channel set type read in the channel set block header is not referenced, 0 is return ("Unknown")

        Args:
            ch_set_nb (int): Absolute nb of the channel set of the trace

        Returns:
            int: SEG-Y trace identification code
        """
        type = self._get_header_value("channel_type", chset_index)
        if type in channel_set_type:
            return channel_set_type[type]["segy_tr_id"]
        else:
            return 0

    def _get_segy_data_use(self):
        """Method that returns the SEG-Y data use according in function of the SEG-D record type (gh1),
        conversion made through the record_type dictionnary (see constants.py)

        If the record type read in gh1 is not referenced, 0 is returned

        Returns:
            int: SEG-Y Data use

        """
        rec_type = self._get_header_value("record_type")
        if rec_type in record_type:
            return record_type[rec_type]["segy_data_use"]
        else:
            return 0

    def _get_file_name_print(self):
        """Method that returns filename for log purpose. Deals with case segd is a bytestream

        Returns:
            str: file name to print (TIF8 file and ffs if SEG-D container is TIF8; SEG-D path if SEG-D file is path
        """

        if self.container:
            return "%s - ffs: %i -" % (self.container, self.ffs)
        else:
            return os.path.basename(self.filename)


def merge_to_segy(output_file=None, string_ebcdic=None, ascii_textual=False, file_ebcdic=None, format_code=5,
                  resample=None, big_endian=True, sorting_code=1, segy_rev=1, extended_textual_header=0,
                  binary_header_dict=None, limit_tr_0=1, limit_tr_1=None, paths=None):
    list_segd_file = []
    for path in paths:
        for p in walk_files(path):
            list_segd_file.append(p)
    list_segd_file = sorted(list_segd_file)

    if not output_file:
        output_file = list_segd_file[0].split('/')[:-1][0] + 'merge' + EXT_SEGY

    with open(output_file, 'wb+') as segy_file:
        segy_file.write(ebcdic(string=string_ebcdic, file_input=file_ebcdic, ascii=ascii_textual))
        segy_file.write(400 * ' ')

        aux_traces = 0
        tr_nb_within_file = 1
        sample_pattern_ref = None
        sample_rate = 0
        # pbar = ProgressBar(maxval=list_segd_file[-1],
        #                    widgets=['Merge SEG-D files ', Bar('#', '[', ']'), ' ', Percentage(), ' ', ETA()])
        # pbar.start()
        for i in list_segd_file:
            print i
            segd_file = Segd(i)
            aux_traces += segd_file.get_aux_traces()
            sample_pattern = segd_file.get_nb_samples_pattern()
            if not sample_pattern_ref:
                sample_pattern_ref = sample_pattern
                sample_rate = segd_file.sample_rate
            elif sample_pattern[1] != sample_pattern_ref[1] or sample_pattern[0] != sample_pattern_ref[0]:
                sample_pattern_ref = (sample_pattern_ref[0], 0)

            if not limit_tr_1:
                limit_tr_1 = segd_file.total_traces

            segd_file.write_segy_traces(segy_file,
                                        big_endian=big_endian,
                                        resample=resample,
                                        trace_sorting_code=sorting_code,
                                        format=format_code,
                                        limit0=limit_tr_0,
                                        limit1=limit_tr_1,
                                        first_trace_nb=tr_nb_within_file)
            if not limit_tr_1:
                limit_tr_1 = segd_file.total_traces
            tr_nb_within_file += limit_tr_1 - limit_tr_0 + 1
            # pbar.update(i)

        segy_file.seek(EBCDIC_HEADER_LENGTH)
        segy_file.write(binary(create_binary_dict(format=format_code,
                                                  trace_sorting_code=sorting_code,
                                                  segy_rev=segy_rev,
                                                  extended_textual_header=extended_textual_header,
                                                  binary_header_dict=binary_header_dict,
                                                  aux_tr=aux_traces,
                                                  total_traces=tr_nb_within_file - 1,
                                                  nb_samples_pattern=sample_pattern_ref,
                                                  sample_rate=sample_rate)))

        # pbar.finish()
        print "File %s created" % output_file


def create_binary_dict(format=5, trace_sorting_code=1, segy_rev=1, extended_textual_header=0,
                       binary_header_dict=None, aux_tr=None, total_traces=None, nb_samples_pattern=None,
                       sample_rate=None):
    """
        Create a 'minimum' binary dictionnary composed by the value of the mandatory fields of
        the binary header read from the object's attributes or from the convert_to_segy method's arguments.
        Possibility to overwrite 'structural' values with the binary_header_dict passed as arguments

        Returns:
            dict:
            key = Name of the header (This name must be part of the headers set in the binary_header dictionnary
            (see constants_sgy.py)
            value = raw value (will be packed in binary function)
        """
    binary_dict = dict()
    if binary_header_dict:
        for header, value in binary_header_dict.iteritems():
            binary_dict[header] = value
    if "traces_per_ensemble" not in binary_dict:
        binary_dict["traces_per_ensemble"] = total_traces
        # binary_dict["traces_per_ensemble"] = 65535
    if "aux_traces_per_ensemble" not in binary_dict:
        binary_dict["aux_traces_per_ensemble"] = aux_tr
    if "nb_samples_per_trace" not in binary_dict:
        binary_dict["nb_samples_per_trace"] = nb_samples_pattern[0]
    if "fixed_length_flag" not in binary_dict:
        binary_dict["fixed_length_flag"] = nb_samples_pattern[1]
    if "sample_interval" not in binary_dict:
        binary_dict["sample_interval"] = sample_rate * 1000
    if "format_code_segy" not in binary_dict:
        binary_dict["format_code_segy"] = format
    if "trace_sorting" not in binary_dict:
        binary_dict["trace_sorting"] = trace_sorting_code
    if "segy_rev" not in binary_dict:
        binary_dict["segy_rev"] = segy_rev
    if "extended_textual_header" not in binary_dict:
        binary_dict["extended_textual_header"] = extended_textual_header
    return binary_dict


def hexdump(src, length=16, offset_decimal=True):
    lead0_offset = len(str(len(src)))
    filter = ''.join([(len(repr(chr(x))) == 3) and chr(x) or '.' for x in range(256)])
    lines = []
    for c in xrange(0, len(src), length):
        chars = src[c:c + length]
        hex = ' '.join(["%02x" % ord(x) for x in chars])
        printable = ''.join(["%s" % ((ord(x) <= 127 and filter[ord(x)]) or '.') for x in chars])
        if offset_decimal:
            lines.append("%s  %-*s  %s\n" % (str(c).zfill(lead0_offset), length * 3, hex, printable))
        else:
            lines.append("%04x  %-*s  %s\n" % (c, length * 3, hex, printable))
    return ''.join(lines)


if __name__ == '__main__':
    f = Segd('/home/thibault/PycharmProjects/segd/samples/segd_cuba/00000988.segd')
    # f = Segd('/home/thibaultuzu/PycharmProjects/segd/samples/segd_cuba/00000988.segd')
    f.info()
    f.info_gh1()
    f.info_gh2()
    f.info_ghn()
    f.add_header("line", block_header="extended_header", rel_offset=784, type='s16', strip=True)
    f.print_header_value("line")
    f.info_channel_set()
    f.dump_block_header("gh2")
    # f.qc(limit0=3867, limit1=3869)
    # f.qc(limit0=5, limit1=78)
    f.qc()
    # f = Segd('/home/thibaultuzu/Desktop/samples/samples_OGDR/seismic/2D_FIELDSEGD/extract/1634.segd')
    # f.convert_to_segy(file_ebcdic='/home/thibaultuzu/PycharmProjects/segd/ebcdic_28_lines.txt',
    #                   output_file='/home/thibaultuzu/Desktop/samples/samples_OGDR/seismic/2D_FIELDSEGD/1634.sgy')

    f.convert_to_segy(output_file='%s.sgy' % f.ff,
                      file_ebcdic='/home/thibault/PycharmProjects/segd/ebcdic_28_lines.txt')
    # f.convert_to_segy(output_file='test_conv.sgy',
    #                   file_ebcdic='/home/thibaultuzu/PycharmProjects/segd/ebcdic_28_lines.txt', ascii_textual=True)
    # f = Segd('/home/thibaultuzu/Desktop/samples/samples_OGDR/2D_FIELDSEGD/extract/1739.segd')
    # f.info()
    # f.convert_to_segy(output_file='%s.sgy' % f.ff,
    #                   file_ebcdic='/home/thibaultuzu/PycharmProjects/segd/ebcdic_28_lines.txt')

    # indir = '/home/thibaultuzu/Desktop/samples/samples_OGDR/3D_FIELDSEGD/extract/'
    # indir = '/home/thibaultuzu/Desktop/GDF_Project/segd_to_segy/Field_Data_SEG-D/STMBHAST 8904/FA06O0017/Tape2Disk/'
    # merge_to_segy(output_file='/home/thibaultuzu/Desktop/GDF_Project/segd_to_segy/STEMMERBERG-HASTE_8904_segd_to_segy_merge.segy',
    #               file_ebcdic='/home/thibaultuzu/Desktop/GDF_Project/segd_to_segy/EBCDIC.txt',
    #               paths=[indir])
    # for root, dirs, filenames in os.walk(indir):
    #     for f in filenames:
    #         a = Segd(indir + f)
    #         # a.info()
    #         a.convert_to_segy(
    #             output_file='/home/thibaultuzu/Desktop/GDF_Project/segd_to_segy/export/ffs_%s_ff_%s.sgy' % (f.zfill(
    # 3), str(a.ff).zfill(4)),
    #             file_ebcdic='/home/thibaultuzu/PycharmProjects/segd/ebcdic_28_lines.txt')
    #         a.close()
    # f = Segd('/home/thibaultuzu/Desktop/samples/samples_OGDR/seismic/3D_FIELDSEGD/extract/108447.segd')
    # f.dump_block_header("gh1")
    # f.dump_block_header("gh2")
    # f.dump_block_header("extended_header")
    # f.dump_block_header("external_header")
    # f.qc()
    # f.info()
