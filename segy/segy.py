import os
from pack_unpack import unpack, pack
from pyproj import Proj, transform
from shapely.geometry import Point, MultiPoint
from shapely.wkt import loads
import datetime
# from progressbar import ProgressBar, Bar, Percentage, ETA
import textwrap
from segy_utils import *
#import numpy as np

# from progressbar import ProgressBar, Bar, Percentage, ETA


EXT_SEGY = '.sgy'
EBCDIC_HEADER_SIZE = 3200
BINARY_HEADER_SIZE = 400
EBCDIC_LINE_LENGTH = 80
EBCDIC_LINES = 40
TH_SIZE = 240


# TODO: review unpack
# TODO: add docstring, doctest
# TODO: unittest
# TODO: handling exceptions, try/except, errors
# TODO: logging
# TODO: command line
# TODO: command line/home/raj/Downloads/SeiSee_2_22_6.exe

class SegyFormatError(Exception):
    pass


class Segy:
    def __init__(self, filename=None, bytestream=None):
        self.bytestream = bytestream
        self.filename = self._get_filename(filename)
        self.open_segy = self.open(filename, bytestream)
        if not self.open_segy:
            raise SegyFormatError("Error: Invalid or corrupt SEG-Y File")
        self.size = self._get_size(filename, bytestream)
        if not self._test_segy():
            raise SegyFormatError("Error: Invalid or corrupt SEG-Y File")
        self.ebcdic = self._set_ebcdic()
        try:
            self.binary_header = self._set_binary()
        except ValueError:
            raise SegyFormatError
        if not self._test_segy():
            raise SegyFormatError("Error: Invalid or corrupt SEG-Y File")
        self.sample_rate = self._get_sample_rate()
        self.samples_per_trace = self._get_sample_per_trace()
        self.record_length = self._set_record_length()
        self.sample_format = self._get_sample_format()
        self.sample_size = self._get_sample_size()
        self.trace_size = self._set_trace_size()
        self.total_traces = self._get_total_traces()
        self.total_traces_real = 0
        self.trace_map = {}
        self.read_nb_samples_from_binary = False
        self.srid = None
        self.posX = None
        self.length_posX = None
        self.posY = None
        self.length_posY = None
        self.fileReadingStatus = 'SUCCESS'
        self.fileReadingError = ''
        self.minPntCut = 0
        self.maxPntCut = 0
        self.tr_written = 0


    # TODO: check if file already opened first
    @staticmethod
    def open(filename, bytestream=None):
        """
        Returns open file object if the input of the class is a file,
        else returns None

        Args:
            filename (str): Filename of the file to open
            bytestream (bool): True if input of the class is a byte stream

        Returns:
            Open file object
        """
        if bytestream:
            return None
        else:
            try:
                return open(filename, 'r+b')
            except IOError:
                print "No such file or directory: '%s'" % filename
                return False

    def close(self):
        if self.filename:
            self.open_segy.close()
            return
        else:
            return None

    @staticmethod
    def _get_filename(filename):
        """
        Returns the filename of the object if exists

        Returns:
            str: filename of the object
        """
        file_name = None
        if filename:
            file_name = filename
        return file_name

    @staticmethod
    def _get_size(filename, bytestream):
        """
        Returns size of the Segy object in function of the input (bytestream or file)

        Returns:
            int: size of the SEG-D object
        """
        if bytestream:
            return len(bytestream)
        else:
            return os.path.getsize(filename)

    def _read(self, pos, length):
        # type: (object, object) -> object
        """
        Read data into segy object.
        If our object is a bytestream (defined by the object attribute filename), just extract the range of bytes
        at the good offset from the bytestream.
        elif our object is a file, opens it, seeks to the absolute offset and read the range of data.

        Args:
            pos (int): Offset of the data to read
            length (int): Length of the data to read

        Returns:
            int: bytestream of th data
        """
        if self.open_segy:
            self.open_segy.seek(pos)
            data = self.open_segy.read(length)
        else:
            data = self.bytestream[pos:pos + length]
        return data

    def _write(self, pos, bytestream):
        """
        Write data into segy object.

        Args:
            pos (int): Offset of the data to read
            bytestream (str): Bytestream to write to the Segy object

        Returns:
            None: As per IO write() method
        """
        length = len(bytestream)
        if self.open_segy:
            self.open_segy.seek(pos)
            return self.open_segy.write(bytestream)
        else:
            self.bytestream[pos:pos + length] = bytestream
            return None

    def _read_header_value(self, pos=None, length=None, type=None):
        """Read the header defined by position, length and type into the file.
        The raw data is then sent to the unpack method to return the value in function of the type.

        Args:
            header (str): Name of the header. This name must be part of the headers set in the segd dictionnary (see
            constants.py) or one of the created header (see add_header method)

            pos (optional, int): In case of the trace header the position has already been set through index_trace
            method and stored in self.trace_map object

            length (optional, int): Not used at the moment, but keep the possibility to manually attribute a value to
            the length (in bytes)

        Returns:
            Value of the header (int, float, str)
        """
        data = self._read(pos, length)
        return unpack(data, type)

    def _test_segy(self):
        if self.size <= 3840:
            # print "File corrupted (size < 3840)"
            return False
        elif hasattr(self, 'binary_header'):
            if get_header_value("format_code_segy", self.binary_header) not in range(1, 9) \
                    or get_header_value("sample_interval", self.binary_header) <= 0 \
                    or get_header_value("sample_interval", self.binary_header) <= 0:
                return False
            else:
                return True
        else:
            return True

    def _set_ebcdic(self):
        """Init method that returns the ebcdic header.

        Returns:
            str: EBCDIC header (raw, 3200 bytes)
        """
        ebcdic = self._read(0, EBCDIC_HEADER_SIZE)
        return ebcdic

    def _set_binary_header_raw(self):
        """Method that returns the binary header.

        Returns:
            str: Binary Header (raw, 400 bytes)
        """
        binary = self._read(EBCDIC_HEADER_SIZE, BINARY_HEADER_SIZE)
        return binary

    def _set_binary(self):
        """Init method that returns the binary header dictionnary.
        Copy the segy binaru header dictionnary (see constants. py) (headers, and for each header: code, pos, type,
        human name)
        and for each header attributes the value read into the file/bytestream through the _read_header_value method

        Returns:
            dict: gh1 dictionnary with read values
        """
        bh = copy.deepcopy(binary_header)
        for header in bh.iterkeys():
            pos = bh[header]["pos"] + EBCDIC_HEADER_SIZE
            type = bh[header]["type"]
            length = get_range(type)
            bh[header]["value"] = self._read_header_value(pos, length, type)
        return bh

    def _get_sample_rate(self):
        """Init method that returns the sample rate (in microseconds) from the binary header (bytes 17-18).
        Returns:
            int: Sample interval (microseconds)
        """
        return int(self.binary_header["sample_interval"]["value"])

    def _get_sample_per_trace(self):
        """Init method that returns the number of samples per trace from the binary header (bytes 21-22).
        Returns:
            int: Number of samples per trace
        """
        return int(self.binary_header["nb_samples_per_trace"]["value"])

    def _set_record_length(self):
        """Init method that returns the record length in milliseconds.
        Returns:
            float: Record length (milliseconds)
        """
        rl = self.samples_per_trace * self.sample_rate / 1000
        return rl

    def _get_sample_format(self):
        """Init method that returns the sample format from the binary header (bytes 25-26).
        Returns:
            int: Sample Format
        """
        return int(self.binary_header["format_code_segy"]["value"])

    def _get_sample_size(self):
        """Init method that returns the sample size in function of the sample format.
        Returns:
            int: Sample Size
        """
        return sample_format[self.sample_format]["size"]

    def _set_trace_size(self):
        """Init method that returns the trace size in function of the sample size and the number of samples per trace
        Returns:
            int: Trace size (byte)
        """
        return self.sample_size * self.samples_per_trace + TH_SIZE

    def _get_total_traces(self):
        """Init method that returns the calculated total of traces in function of the physical size of the Segy
        object and the trace size
        Returns:
            float: Total traces
        """
        size_data_block = self.size - EBCDIC_HEADER_SIZE + BINARY_HEADER_SIZE
        return size_data_block / self.trace_size

    def read_trace(self, tr_nb):
        """Method that returns the bytestream of the trace

        Args:
            tr_nb (int): Trace number

        Returns:
            str: Trace data
        """
        offset = self.trace_map[tr_nb]["offset"]
        size = self.trace_map[tr_nb]["trace_size"]

        trace = self._read(offset, size)
        return trace

    def _set_trace_map(self, list_headers=None, trace_header_dictionary=None):
        """Method that creates the trace map (trace number, offset, trace_size), given headers value stored in a hash.

        Args:
            list_headers (Array): list of headers value that we want to add to the trace map hash

        Returns:
            hash of hash: Trace Map hash where key are integer value of the trace number (from file structure,
            no trace header). Minimal keys are: offset, nb_samples_tr, trace_size
        """
        # pbar = ProgressBar(maxval=self.total_traces,
        #                    widgets=['Index SEG-Y file ', Bar('#', '[', ']'), ' ', Percentage(), ' ', ETA()])
        # pbar.start()

        if not trace_header_dictionary:
            th = copy.deepcopy(trace_header)
        else:
            th = trace_header_dictionary
        tr_nb = 1
        offset0 = EBCDIC_HEADER_SIZE + BINARY_HEADER_SIZE

        if self.trace_map == {}:
            self.trace_map = {tr_nb: {"offset": offset0}}

        if list_headers and list_headers != ["all"]:
            header_iterator = list_headers
        else:
            header_iterator = th.keys()

        # Need to have imperatively nb_samples_tr header value to calculate the trace size. Consequently it is added
        # to the header_iterator set
        header_iterator.append("nb_samples_tr")
        header_iterator = set(header_iterator)
        # tr_nb can be called as header from polycut function. But because it is not in the the trace_header
        # dictionnary we have to remove it or we'll trigger an error in the iteration over the trace header trace hash.
        # the key, value will be added later during the iteration
        if "tr_nb" in header_iterator:
            header_iterator.remove("tr_nb")

        while True:
            for header in header_iterator:
                pos = th[header]["pos"] + offset0
                # print "%i,%s,%i,%i,%i" %(tr_nb, header, trace_map[tr_nb]["offset"], offset0, pos)
                type = th[header]["type"]
                length = get_range(type)
                # Read header value only if header value not in self.trace_map

                if header not in self.trace_map[tr_nb]:
                    try:
                        self.trace_map[tr_nb][header] = {"type": th[header]["type"],
                                                         "human_name": th[header]["human_name"],
                                                         "pos": th[header]["pos"],
                                                         "value": self._read_header_value(pos, length, type)
                                                         }
                    except:
                        # print "pb read trace header: %s - tr_nb: %i" % (header, tr_nb)
                        # EOF reached
                        self.trace_map.pop(tr_nb)
                        self.total_traces_real = tr_nb - 1
                        # pbar.finish()
                        self.fileReadingStatus = 'SUCCESS'
                        return self.trace_map

                    if self.trace_map[tr_nb][header]["value"] == 0:
                        ##if header != 'nb_samples_tr' and  self._read_header_value(offset0 + 26, 4, 'N4') == 1:    # Reading Trace id value if pter than one then continue
                        if header != 'nb_samples_tr' and self._read_header_value(offset0 + th[header]["pos"], 4,'N4') == 1:  # Reading Trace id value if pter than one then continue
                            self.fileReadingStatus = 'UNSUCCESS'
                            self.fileReadingError="Finding Trace number value 0;"
                            return self.trace_map

            # if tr_nb % 10000 == 0:
            #     print "traces %i/%i read\r" % (tr_nb, self.total_traces)
            # pbar.update(tr_nb)
            # Read header value only if header value not in self.trace_map (condition on trace_size part of minimal
            #  keys)
            if "trace_size" not in self.trace_map[tr_nb]:
                if self.read_nb_samples_from_binary:
                    trace_size = self.binary_header["nb_samples_per_trace"]["value"] * self.sample_size + TH_SIZE
                else:
                    if self.trace_map[tr_nb]["nb_samples_tr"]["value"] <= 0 :
                        #due to negative value of tracedate , Reading sample value from binary header
                        trace_size = self.binary_header["nb_samples_per_trace"]["value"] * self.sample_size + TH_SIZE
                    else:
                        trace_size = self.trace_map[tr_nb]["nb_samples_tr"]["value"] * self.sample_size + TH_SIZE

                self.trace_map[tr_nb]["trace_size"] = trace_size
                self.trace_map[tr_nb]["tr_nb"] = tr_nb

                # Init key for next trace
                tr_nb += 1
                offset0 += trace_size
                self.trace_map[tr_nb] = {"offset": offset0}

                if tr_nb > self.total_traces + 10:
                    self.fileReadingStatus='UNSUCCESS'
                    self.fileReadingError = 'File Reading traces more then total count,Plz Check Sample size or other error;'
                    return self.trace_map

                # if tr_nb > 21501:
                #     print tr_nb

                # if pos == 111298264 or pos == 111294020 or pos ==111289776:
                #     print('Zoal')
                #
                # if pos > 112995850:
                #     print(pos)

            elif "trace_size" in self.trace_map[tr_nb] and (tr_nb + 1) not in self.trace_map:
                offset0 += self.trace_map[tr_nb]["trace_size"]
                tr_nb += 1
                self.trace_map[tr_nb] = {"offset": offset0}
            elif tr_nb == self.total_traces_real:
                # pbar.finish()
                self.fileReadingStatus = 'SUCCESS'
                return self.trace_map
            else:
                tr_nb += 1
                offset0 += self.trace_map[tr_nb]["trace_size"]

        # pbar.finish()
        self.fileReadingStatus = 'SUCCESS'
        return self.trace_map

    def _set_trace_map_tr_nb(self, list_tr_nb=None, list_header=None):
        """Method used for polycut, trace_header, _get_trace_header_value_array methods with header 'tr_nb' to avoid
        to scan the whole file.
        Args:
            list_tr_nb (Array): list of tr_nb value that we want to add to the trace map hash
            list_header (Array): list of header keywords we want to parse
        Returns:
            hash of hash: Trace Map hash where key are integer value of the trace number (from file structure,
            no trace header). Keys are: offset, nb_samples_tr, trace_size
        """
        # pbar = ProgressBar(maxval=self.total_traces,
        #                    widgets=['Index SEG-Y file ', Bar('#', '[', ']'), ' ', Percentage(), ' ', ETA()])
        # pbar.start()

        tr_nb = 1
        offset0 = EBCDIC_HEADER_SIZE + BINARY_HEADER_SIZE

        if self.trace_map == {}:
            self.trace_map = {tr_nb: {"offset": offset0}}

        th = copy.deepcopy(trace_header)

        max_tr_nb = max(list_tr_nb)
        if not list_header:
            header_iterator = ["nb_samples_tr"]
            header_iterator = set(header_iterator)
        elif list_header == ["all"]:
            header_iterator = th.keys()
        else:
            header_iterator = list_header
            header_iterator.append("nb_samples_tr")
            header_iterator = set(header_iterator)
        # tr_nb can be called as header from polycut function. But because it is not in the the trace_header
        # dictionnary we have to remove it or we'll trigger an error in the iteration over the trace header trace hash.
        # the key, value will be added later during the iteration
        if "tr_nb" in header_iterator:
            header_iterator.remove("tr_nb")

        while True:
            for header in header_iterator:
                pos = th[header]["pos"] + offset0
                # print "%i,%s,%i,%i,%i" %(tr_nb, header, trace_map[tr_nb]["offset"], offset0, pos)
                type = th[header]["type"]
                length = get_range(type)
                # Read header value only if header value not in self.trace_map
                if header not in self.trace_map[tr_nb]:
                    try:
                        self.trace_map[tr_nb][header] = {"type": th[header]["type"],
                                                         "human_name": th[header]["human_name"],
                                                         "pos": th[header]["pos"],
                                                         "value": self._read_header_value(pos, length, type)
                                                         }
                    except:
                        # EOF reached
                        # print "pb read trace header: %s - tr_nb: %i" % (header, tr_nb)
                        self.trace_map.pop(tr_nb)
                        self.total_traces_real = tr_nb - 1
                        # pbar.finish()
                        return self.trace_map
            # pbar.update(tr_nb)
            # Read header value only if header value not in self.trace_map (condition on trace_size part of minimal
            #  keys)
            if "trace_size" not in self.trace_map[tr_nb]:
                if self.read_nb_samples_from_binary:
                    trace_size = self.binary_header["nb_samples_trace"]["value"] * self.sample_size + TH_SIZE
                else:
                    trace_size = self.trace_map[tr_nb]["nb_samples_tr"]["value"] * self.sample_size + TH_SIZE

                self.trace_map[tr_nb]["trace_size"] = trace_size
                self.trace_map[tr_nb]["tr_nb"] = tr_nb
                if tr_nb == max_tr_nb:
                    # pbar.finish()
                    return self.trace_map
                # Init key for next trace
                tr_nb += 1
                offset0 += trace_size
                self.trace_map[tr_nb] = {"offset": offset0}
            elif tr_nb == max_tr_nb:
                # pbar.finish()
                return self.trace_map
            else:
                tr_nb += 1
                offset0 += self.trace_map[tr_nb]["trace_size"]

        # pbar.finish()
        return self.trace_map

    def spatial_polycut(self, wkt, output_file, crsIdPolygon=4326, crsIdSegy=4326, posX=180, posY=184, lengthX=4,
                        lengthY=4, coordMultiplier=1):
        """
        """
        polygon = None
        try:
            polygon = loads(wkt)
        except:
            print "Bad wkt string"
            exit
        if not self.srid:
            self.srid = crsIdSegy
        if not self.posX:
            self.posX = posX
        if not self.posY:
            self.posY = posY
        if not self.length_posX:
            self.length_posX = lengthX
        if not self.length_posY:
            self.length_posX = lengthY

        # We add the posX and posY to the trace header dictionary this way any non standard trace header mapping can
        # be handled
        th = copy.deepcopy(trace_header)
        th['posX'] = {"pos": posX, "type": 'N' + str(lengthX), "human_name": "Custom X position for spatial_polycut"}
        th['posY'] = {"pos": posY, "type": 'N' + str(lengthY), "human_name": "Custom Y position for spatial_polycut"}

        # Trace all Input file information , added posx, posy and scaler information to get graphic pioint.
        self.trace_map = self._set_trace_map(["posX", "posY", "scalar_coord"], th)

        # Run Spatial query for Every point  and return list of spatial point.
        list_tr_nb = self.trace_in_polygon(polygon, crsIdPolygon, crsIdSegy, coordMultiplier)
        print "%s - %s - %i traces found in polygon" % (datetime.now(), self.filename, len(list_tr_nb))

        if list_tr_nb:
            self.polycut("tr_nb", list_tr_nb, output_file)
        else:
            print "%s - %s - No traces are within the given polygon" % (datetime.now(), self.filename)

        return

    def trace_in_polygon(self, polygon, crsIdPolygon, crsIdSegy, coordMultiplier):

        wktProj = Proj(init='epsg:' + str(crsIdPolygon))
        segyProj = Proj(init='epsg:' + str(crsIdSegy))
        list_tr_nb = []

        coordMultiplier = self.coord_multiplier_parser(coordMultiplier)

        for tr_nb, trace_header in self.trace_map.iteritems():
            coordMultiplier_tr = self.coord_multiplier_parser(trace_header["scalar_coord"]["value"])
            coordX = trace_header['posX']["value"] * coordMultiplier_tr * coordMultiplier
            coordY = trace_header['posY']["value"] * coordMultiplier_tr * coordMultiplier

            if crsIdSegy != crsIdPolygon:
                coord_traceX, coord_traceY = transform(segyProj, wktProj, coordX, coordY)
            else:
                coord_traceX, coord_traceY = coordX, coordY

            coord_trace = Point(coord_traceX, coord_traceY)

            if coord_trace.within(polygon):
                list_tr_nb.append(tr_nb)

        return list_tr_nb

    def wkt_outline(self, crsIdWkt=None, crsIdSegy=4326, posX=180, posY=184, lengthX=4,
                    lengthY=4, coordMultiplier=1):

        if not crsIdWkt:
            crsIdWkt = crsIdSegy
        if not self.srid:
            self.srid = crsIdSegy
        if not self.posX:
            self.posX = posX
        if not self.posY:
            self.posY = posY
        if not self.length_posX:
            self.length_posX = lengthX
        if not self.length_posY:
            self.length_posX = lengthY

        wktProj = Proj(init='epsg:' + str(crsIdWkt))
        segyProj = Proj(init='epsg:' + str(crsIdSegy))
        # We add the posX and posY to the trace header dictionary this way any non standard trace header mapping can
        # be handled
        th = copy.deepcopy(trace_header)
        th['posX'] = {"pos": posX, "type": 'N' + str(lengthX), "human_name": "Custom X position for convexHull"}
        th['posY'] = {"pos": posY, "type": 'N' + str(lengthY), "human_name": "Custom Y position for convexHull"}

        self.trace_map = self._set_trace_map(["posX", "posY", "scalar_coord"], th)

        listPoint = []

        for tr_nb, th in self.trace_map.iteritems():
            coordMultiplier_tr = self.coord_multiplier_parser(th["scalar_coord"]["value"])
            coordX = th['posX']["value"] * coordMultiplier_tr * coordMultiplier
            coordY = th['posY']["value"] * coordMultiplier_tr * coordMultiplier

            # Remove odd trace header values
            if coordX != 0 and coordY != 0:
                if crsIdWkt != crsIdSegy:
                    coord_traceX, coord_traceY = transform(segyProj, wktProj, coordX, coordY)
                else:
                    coord_traceX, coord_traceY = coordX, coordY

                listPoint.append([coord_traceX, coord_traceY])

        point_collection = MultiPoint(list(listPoint))
        convex_hull_polygon = point_collection.convex_hull
        print convex_hull_polygon
        return convex_hull_polygon

    # def area(self, crsIdWkt=None, crsIdSegy=4326, posX=180, posY=184, lengthX=4,
    #                 lengthY=4, coordMultiplier=1):
    # convex

    @staticmethod
    def coord_multiplier_parser(coord_multiplier):
        if coord_multiplier < 0:
            return 1 / abs(float(coord_multiplier))
        else:
            return coord_multiplier

    def polycut(self, header, list_to_extract, output_file, ebcdic=None):
        # Create array of sorted keys (key = list of items nb (cdp, tr nb, sp, il, xl, etc..) to extract from the segy)
        keys = []
        for serie in list_to_extract:
            try:
                keys.extend(serie)
            except TypeError:
                keys.append(serie)
        keys = sorted(keys)
        keys = set(keys)

        if header == "tr_nb":
            # In this case, no need to scan all the traces as tr_nb is structural and we don't need to search into
            # trace header (performance optimization)
            self.trace_map = self._set_trace_map_tr_nb(keys)
        else:
            self.trace_map = self._set_trace_map([header])
        if not ebcdic:
            ebcdic = self._set_ebcdic()
        sgy_output = ebcdic
        sgy_output += self._set_binary_header_raw()

        # Array of sorted trace number to navigate in trace_map in a sorted way
        traces = sorted(self.trace_map.keys())

        fstPntFlg= False

        tr_written = 0
        with open(output_file, 'w+') as fout:
            fout.write(sgy_output)
            for trace in traces:
                try:
                    if header == 'tr_nb':
                        if self.trace_map[trace][header] in keys:
                            tr_written += 1
                            # Write good trace number (byte 1-4 trace header)
                            trace_data = pack(tr_written, 'N4')
                            trace_data += self.read_trace(trace)[4:]
                            fout.write(trace_data)
                    else:
                        if self.trace_map[trace][header]['value'] in keys:
                            if fstPntFlg == False:
                                self.minPntCut = self.trace_map[trace][header]['value']
                                fstPntFlg = True

                            self.maxPntCut = self.trace_map[trace][header]['value']

                            tr_written += 1
                            # Write good trace number (byte 1-4 trace header)
                            trace_data = pack(tr_written, 'N4')
                            trace_data += self.read_trace(trace)[4:]
                            fout.write(trace_data)
                except KeyError:
                    next
            # Write good number of traces in binary (bytes 13-14 binary header)
            fout.seek(3212)
            if tr_written < 32768:
                fout.write(pack(tr_written, 'N2'))
            else:
                fout.write(pack(0, 'N2'))
        #
        #Set class trace written values
        self.tr_written = tr_written
        if tr_written == 0:
            print "%s - %s - No matching" % (datetime.now(), self.filename)
            os.remove(output_file)
        else:
            print "%s - %s - %i traces written to %s" % (datetime.now(), self.filename, tr_written, output_file)
            print "%s - %s - No matching" % (datetime.now(), self.filename)

    def edit_ebcdic(self, file_input=None, ascii=False):
        """
        Edit the first 3200 bytes of the Segy Object from a file. Finally replace the original file with modified
        EBCDIC header.

        Args:
            file_input (str): File path of the new EBCDIC header
            ascii (boolean): Encode EBCDIC header in Ascii if True

        Returns:
            Overwrite Segy file EBCDIC header
        """
        ebcdic_header = create_ebcdic(string=None, file_input=file_input, ascii=ascii)
        self.ebcdic = ebcdic_header
        return self._write(0, self.ebcdic)

    def edit_binary(self, header_key_word, value):
        """
        Edit Binary Header value.

        Args:
            header_key_word (str): Binary Header Key Word
            value (int): Binary header new value

        Returns:
            Overwrite Segy file EBCDIC header
        """
        if header_key_word not in self.binary_header.iterkeys():
            print "Header: %s not in Binary Header" % header_key_word
            return
        pos = get_header_position(header_key_word, binary_header) + EBCDIC_HEADER_SIZE
        value_header = pack(value, self.binary_header[header_key_word]["type"])
        return self._write(pos, value_header)

    def edit_trace_header(self, header_key_word, value, sequential=False, incr=1):
        """
        Edit Trace Header value.
        Can be edited sequentially if option selected.

        Args:
            header_key_word (str): Binary Header Key Word
            value (int): Binary header new value

        Returns:
            Overwrite Segy file EBCDIC header
        """
        th = copy.deepcopy(trace_header)
        if header_key_word not in th.iterkeys():
            print "Header: %s not in Trace Header" % header_key_word
            return
        self.trace_map = self._set_trace_map(["tr_nb"])
        value0 = value
        for v in self.trace_map.itervalues():
            pos_trace = v["offset"]
            pos_header = get_header_position(header_key_word, trace_header)
            pos = pos_trace + pos_header
            if sequential:
                value_trace = value0 + incr
            else:
                value_trace = value
            value_header = pack(value_trace, trace_header[header_key_word]["type"])
            value0 = value_trace
            self._write(pos, value_header)
        return

    def info(self):
        """Print common info of the Segy object"""
        buf = "\nSummary\n\n"
        buf += "File Name            : %s\n\n" % (self.filename if self.filename else 'Bytestream')
        buf += "File Size            : %i\n\n" % self.size
        buf += "# Traces             : %s\n" % self.total_traces
        buf += "# Samples per traces : %s\n" % self.samples_per_trace
        buf += "Samples Format       : %i - %s\n" % (self.sample_format, sample_format[self.sample_format]["name"])
        buf += "Sample Interval (uS) : %i\n" % self.sample_rate
        buf += "Record Length (mS)   : %i\n" % self.record_length
        print buf
        return

    def trace_header(self, list_tr_nb=None, list_headers=["all"], output_file=None):
        """Print Trace header information"""

        if list_headers == ["all"]:
            th = copy.deepcopy(trace_header)
            list_headers = th.keys()

        if list_tr_nb:
            keys = []
            for serie in list_tr_nb:
                try:
                    keys.extend(serie)
                except TypeError:
                    keys.append(serie)
            keys = sorted(keys)
            keys = set(keys)
            self.trace_map = self._set_trace_map_tr_nb(keys, list_header=list_headers)
        else:
            self.trace_map = self._set_trace_map(list_headers=list_headers)

        buf = "\nTrace Header - %s\n\n" % (self.filename if self.filename else 'Bytestream')
        j = 0
        for tr_nb, tr in self.trace_map.iteritems():
            if tr == "trace_size":
                continue
            header_pos = [(i, k["pos"], k["human_name"], k["value"]) for i, k in tr.iteritems() if i not in [
                "trace_size",
                "offset",
                "tr_nb"] and i in list_headers]
            ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
            if j == 0:
                buf += "tr_nb,"
                for header in ordered_keys:
                    buf += header[0] + ","
                # buf += ','.join({"%s" % header[2] for header in ordered_keys})
                buf += "\n"
                j += 1
            buf += str(tr_nb) + ","
            for header in ordered_keys:
                buf += str(header[3]) + ","
            # buf += ','.join({"%i" % header[3] for header in ordered_keys})
            buf += "\n"
        if output_file:
            with open(output_file, 'w+') as fout:
                fout.write(buf)
        else:

            print buf
        return

    def print_ebcdic(self):
        """Print ebcdic header of the Segy object"""
        ebcdic_ascii = ''
        try:
            ebcdic_ascii = self.ebcdic.decode('cp500')
        except UnicodeDecodeError:
            try:
                ebcdic_ascii = self.ebcdic.decode('ascii')
            except UnicodeDecodeError:
                print "EBCDIC Header Format not recognized"
                return
        else:
            print "\nEBCDIC Header - %s\n\n" % (self.filename if self.filename else 'Bytestream')
            print textwrap.fill(ebcdic_ascii, 80)
            return

    def print_binary(self):
        """Print ebcdic header of the Segy object"""
        buf = "\nBinary Header - %s\n\n" % (self.filename if self.filename else 'Bytestream')
        header_pos = [(i, k["pos"], k["human_name"]) for i, k in self.binary_header.iteritems() if i != "offset"]
        ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
        buf += ''.join(
            {"%-62s : %-20s\n" % (header[2], get_header_value(
                header[0], self.binary_header)) for header in ordered_keys
             }
        )
        print buf
        return

    def _get_trace_header_value_array(self, header_key_word):
        """Return array of value for a given trace header key word"""

        list_header = [header_key_word]
        self.trace_map = self._set_trace_map(list_headers=list_header)

        header_array = []

        for tr_nb, tr in self.trace_map.iteritems():
            if tr == "trace_size":
                continue
            header_array.append(tr[header_key_word]['value'])
        return header_array


    def header_min(self, header=None):
        """Return minimal value of a given header key word"""

        header_array = np.array(self._get_trace_header_value_array(header))

        return min(header_array)

    def header_max(self, header=None):
        """Return minimal value of a given header key word"""

        header_array = np.array(self._get_trace_header_value_array(header))

        return max(header_array)


    def header_mean(self, header=None):
        """Return average value of a given header key word"""

        header_array = np.array(self._get_trace_header_value_array(header))

        return np.mean(header_array)

    def header_median(self, header=None):
        """Return median value of a given header key word"""

        header_array = np.array(self._get_trace_header_value_array(header))

        return np.median(header_array)


if __name__ == '__main__':
    # f = Segy('/home/thibault/PycharmProjects/segy/samples/ADC50YEMEN-MIGRATION/WG752D0002-OH-17-Final_Migration-Full-251691708.sgy')
    # f.info()
    #
    # f.spatial_polycut(
    #     'POLYGON((-10 58.0923768255, -9.9 50.3, -9 59.5, -10 58.0923768255))',
    #     '/home/thibault/PycharmProjects/segy/samples/ADC50YEMEN-MIGRATION/WG752D0002-OH-17-Final_Migration-Full-251691708.polycut2.sgy',
    #     crsIdSegy=23029
    # )

    # f.spatial_polycut(
    #     'POLYGON((-15 45,15 45,15 60,-15 60, -15 45))',
    #     '/home/thibault/PycharmProjects/segy/samples/ADC50YEMEN-MIGRATION/WG752D0002-OH-17-Final_Migration-Full-251691708.polycut2.sgy',
    #     crsIdSegy=23029
    # )

    # f.polycut("ensemble_number", [range(2005, 2100), 2048, 2064, range(3200, 3204)],
    #           '/home/thibault/PycharmProjects/segy/samples/ADC50YEMEN-MIGRATION/ADCO50YEMEN-1.segy.00020.130209114835'
    #           '.polycut.segy')

    # f.polycut("tr_nb_segy_file", [range(24, 41), 57, range(100, 2600)],
    #         '/home/thibault/PycharmProjects/segy/samples/ADC50YEMEN-MIGRATION/WG752D0002-OH-17-Final_Migration-Full'
    #        '-251691708.polycut.sgy'
    # )
    # f.close()
    #
    # f = Segy('/home/thibault/PycharmProjects/segy/samples/ADC50YEMEN-MIGRATION/WG752D0002-OH-17-Final_Migration-Full-251691708.polycut2.sgy')
    # f.info()
    # f.close()



    #
    # f=Segy('/home/thibault/PycharmProjects/segy/samples/ldr_PDO00JSK1/PDO00JSK1-M-1.segy.00002.110822102437.sgy')
    # f.info()
    # # f.spatial_polycut('POLYGON((57.1528319215676 22.3503194811292,56.9194870553676 22.3503221977341,56.9194901925977 '
    # #                   '22.583644625772,57.1695025666264 22.5836417127177,57.169501438225 22.5003122632565,'
    # #                   '57.2528388942348 22.5003112868849,57.2528377679319 22.4169818520628,'
    # #                   '57.4195126568983 22.4169798954523,57.4195137914234 22.500309326229,'
    # #                   '57.5528537148991 22.500307751697,57.5528480363215 22.0836607183007,'
    # #                   '57.4195081355126 22.083662285239,57.4195094921873 22.1836575489796,'
    # #                   '57.2861695922096 22.1836591116311,57.2861686875961 22.1169956078526,'
    # #                   '57.2028312468843 22.1169965804761,57.2028305734572 22.0669989538616,'
    # #                   '57.2361655492663 22.0669985651681,57.2361646550744 22.0003350692312,'
    # #                   '57.1694947051494 22.0003358427272,57.1528272135594 22.0003360373749,'
    # #                   '57.1528319215676 22.3503194811292))',
    # #                   '/home/thibault/PycharmProjects/segy/samples/ldr_PDO00JSK1/PDO00JSK1-M-1.segy.00002.polycut.segy',
    # #                   crsIdSegy=23240, posX=192, posY=196)
    # f.polycut("ensemble_number", [range(2441, 2600)],
    #         '/home/thibault/PycharmProjects/segy/samples/ldr_PDO00JSK1/PDO00JSK1-M-1.segy.00002.110822102437'
    #         '.polycut.sgy'
    # )

    # f = Segy('/home/thibault/Desktop/projects/egypt/data_sample/1-Q-Land_2008_PSTM_SEG-Y'
    #          '/PSTM_KxKy_GEOSIG_TP_3DRNA_Stacks_Q3D_plus2005_GridInc_4.Segy')
    # f.polycut("tr_nb", [range(1, 3001)],
    #           '/home/thibault/Desktop/projects/egypt/data_sample/1-Q-Land_2008_PSTM_SEG-Y'
    #           '/PSTM_KxKy_GEOSIG_TP_3DRNA_Stacks_Q3D_plus2005_GridInc_4.3000.traces.sgy'
    #           )
    # f.close()
    # f = Segy('/home/thibault/Desktop/projects/egypt/data_sample/1-Q-Land_2008_PSTM_SEG-Y'
    #          '/PSTM_KxKy_GEOSIG_TP_3DRNA_Stacks_Q3D_plus2005_GridInc_4.3000.traces.sgy')
    # f = Segy('/home/thibault/Desktop/projects/ogdr/20170222_OGDR_stationary.xls')
    # f = Segy("/home/thibault/PycharmProjects/segy/samples/YD_15_FULL_PRESDM_T_Final_ABCD_OGDR_BLK48_test.sgy")
    # f = Segy("/home/thibault/CCED_SaiwanI_3D_Final_Stack.segy")
    # f = Segy("/home/thibault/Desktop/BO_3D-Kaimiro-Ngatoro.sgy")
    # trace_header['cdp_extract'] = {"pos": 0, "type": "N4", "human_name": "cdp_test"}
    # f.polycut("cdp_extract", list_to_extract=range(0, 6),
    #           output_file="/home/thibault/Desktop/BO_3D-Kaimiro-Ngatoro.polycut.sgy")

    # f = Segy("/home/thibault/Desktop/projects/ogdr/seismic_export/kaimirio_polycut.sgy")

    # f.info()
    # f.wkt_outline(crsIdSegy=2193, posX=72, posY=76, crsIdWkt=4326)
    # f.print_ebcdic()
    # f.spatial_polycut('POLYGON((174.09350819347128 -39.163929116370156,174.15049977061972 -39.163929116370156,'
    #                   '174.15049977061972 -39.18841457505236,174.09350819347128 -39.18841457505236,174.09350819347128 -39.163929116370156))',
    #                   crsIdSegy=2193, posX=72, posY=76,
    #                   output_file="/home/thibault/Desktop/projects/ogdr/seismic_export/kaimirio_polycut.sgy")
    # f.spatial_polycut('POLYGON((57.1528319215676 22.3503194811292,56.9194870553676 22.3503221977341,56.9194901925977 '
    #                   '22.583644625772,57.1695025666264 22.5836417127177,57.169501438225 22.5003122632565,'
    #                   '57.2528388942348 22.5003112868849,57.2528377679319 22.4169818520628,'
    #                   '57.4195126568983 22.4169798954523,57.4195137914234 22.500309326229,'
    #                   '57.5528537148991 22.500307751697,57.5528480363215 22.0836607183007,'
    #                   '57.4195081355126 22.083662285239,57.4195094921873 22.1836575489796,'
    #                   '57.2861695922096 22.1836591116311,57.2861686875961 22.1169956078526,'
    #                   '57.2028312468843 22.1169965804761,57.2028305734572 22.0669989538616,'
    #                   '57.2361655492663 22.0669985651681,57.2361646550744 22.0003350692312,'
    #                   '57.1694947051494 22.0003358427272,57.1528272135594 22.0003360373749,'
    #                   '57.1528319215676 22.3503194811292))',
    #                   '/home/thibault/PycharmProjects/segy/samples/ldr_PDO00JSK1/PDO00JSK1-M-1.segy.00002.polycut.segy',
    #                   crsIdSegy=23240, posX=192, posY=196)
    # f.polycut("tr_nb", [range(1, 100)],
    #           '/home/thibault/Desktop/projects/egypt/data_sample/1-Q-Land_2008_PSTM_SEG-Y'
    #           '/PSTM_KxKy_GEOSIG_TP_3DRNA_Stacks_Q3D_plus2005_GridInc_4.100.traces.sgy'
    #           )
    # f.spatial_polycut('POLYGON((29.6 30.56, 29.64 30.56, 29.64 30.51, 29.6 30.51, 29.6 30.56))',
    #                   '/home/thibault/Desktop/projects/egypt/data_sample/1-Q-Land_2008_PSTM_SEG-Y'
    #                   '/PSTM_KxKy_GEOSIG_TP_3DRNA_Stacks_Q3D_plus2005_GridInc_4.3000.polycut.traces.sgy',
    #                   crsIdSegy=22992, posX=80, posY=84
    #                   )
    # f.print_ebcdic()
    # f.print_binary()
    # f.trace_header([range(1, 200)], ["tr_nb_line", "ensemble_coord_X", "ensemble_coord_Y"])
    # # f.edit_ebcdic(file_input="/home/thibault/Desktop/projects/password.txt", ascii=True)
    # f.edit_trace_header("offset", 666, True, -1)
    # f.wkt_outline(crsIdWkt=4326, crsIdSegy=22992, posX=80, posY=84)
    # f.close()
    # f = Segy('/home/thibault/Desktop/projects/egypt/data_sample/1-Q-Land_2008_PSTM_SEG-Y'
    #          '/PSTM_KxKy_GEOSIG_TP_3DRNA_Stacks_Q3D_plus2005_GridInc_4.3000.polycut.traces.sgy')
    # f.wkt_outline(crsIdWkt=4326, crsIdSegy=22992, posX=80, posY=84)


    # ------ segy class demo + testing ------ (+ add wkt visualization of il and xl export)
    f = Segy("/home/thibault/Desktop/BO_3D-Kaimiro-Ngatoro.sgy")
    f.info()
    f.print_ebcdic()
    f.print_binary()
    f.trace_header([range(1, 200)], ["tr_nb_line", "ensemble_coord_X", "ensemble_coord_Y"])
    f.wkt_outline(crsIdSegy=2193, posX=72, posY=76, crsIdWkt=4326)

    trace_header['header_extract1'] = {"pos": 12, "type": "N4", "human_name": "header_extract"}
    f.polycut("header_extract1", list_to_extract=[448],
              output_file="/home/thibault/Desktop/BO_3D-Kaimiro-Ngatoro.il448.sgy")
    trace_header['header_extract2'] = {"pos": 16, "type": "N4", "human_name": "header_extract"}
    f.polycut("header_extract2", list_to_extract=[209],
              output_file="/home/thibault/Desktop/BO_3D-Kaimiro-Ngatoro.xl209.sgy")

    f.spatial_polycut(
        'POLYGON((174.09350819347128 -39.163929116370156,174.15049977061972 -39.163929116370156,'
        '174.15049977061972 -39.18841457505236,174.09350819347128 -39.18841457505236,'
        '174.09350819347128 -39.163929116370156))',
        crsIdSegy=2193, posX=72, posY=76, output_file="/home/thibault/Desktop/kaimirio_spatial_polycut.sgy")

    f = Segy("/home/thibault/Desktop/kaimirio_spatial_polycut.sgy")
    f.wkt_outline(crsIdSegy=2193, posX=72, posY=76, crsIdWkt=4326)

    f = Segy("/home/thibault/Desktop/BO_3D-Kaimiro-Ngatoro.xl209.sgy")
    f.trace_header(list_headers=["header_extract1"])
    f.trace_header(list_headers=["header_extract2"])
    print f.header_min(header="header_extract1")
    print f.header_max(header="header_extract1")
    print f.header_min(header="header_extract2")
    print f.header_max(header="header_extract2")
    print f.header_mean(header="header_extract1")
    print f.header_mean(header="header_extract2")
    print f.header_median(header="header_extract1")
    print f.header_median(header="header_extract2")
    # ------------


    f = Segy("/home/thibault/Desktop/BO_3D-Kaimiro-Ngatoro.sgy")
    print f.header_min(header="header_extract1")
    print f.header_max(header="header_extract1")
    print f.header_min(header="header_extract2")
    print f.header_max(header="header_extract2")
    # f = Segy("/home/thibault/MRG02HYM1-M-1.segy.01955.110718154550")
    # f.info()
    # f.trace_header([range(1, 1000)], ["tr_nb_line", "ensemble_coord_X", "in_line"])
    # print f.header_min(header="in_line")
    # print f.header_min(header="tr_nb_segy_file")
    # print f.header_max(header="in_line")
    # print f.header_max(header="tr_nb_segy_file")
    # print f.header_mean(header="in_line")
    # print f.header_mean(header="tr_nb_segy_file")
    # print f.header_median(header="in_line")
    # print f.header_median(header="tr_nb_segy_file")
    # f.first_header_value("")

