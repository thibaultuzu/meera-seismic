#!/usr/bin/env python
# utf-8

import copy
from constants_sgy import binary_header, trace_header, sample_format
from datetime import datetime
from codecs import encode
import os

EBCDIC_HEADER_LENGTH = 3200
EBCDIC_LINE_LENGTH = 80
EBCDIC_LINES = 40


def get_header_human_name(header, dict=None):
    """Return the descr of a header"""
    return dict[header]["human_name"]


def get_header_value(header, dict=None):
    """Return the value of a header"""
    return dict[header]["value"]


def get_header_position(header, dict=None):
    """Return the position of a header"""
    return dict[header]["pos"]


def get_header_type(header, dict=None):
    """Return the type of a header"""
    return dict[header]["type"]


def get_range(type):
    """Returns the length of data defined by a type

    Args:
        type(str): string representation of the word's codification

    Returns:
        int: represents number of bytes
    """
    rango = None
    if type[0] in ['H', 'n', 'N', 'B', 'S', 'E', 's'] and (type[1] == '+' or type[1] == "-"):
        rango = int(float(type[2:])) + 1
    elif type[0] in ['H', 'n', 'N', 'B', 'S', 'E', 's'] and int(type[1]) in range(9):
        rango = int(type[1:])
    return rango


def print_trace_header_key_word():
    """Print list of trace headers"""
    buf = "\nTrace header list ad key words\n\n"
    th = copy.deepcopy(trace_header)
    th["tr_nb"] = {"pos": "N/A", "type": "N2", "human_name": "Trace number within file (not read from trace header)"}
    header_pos = [(i, k["pos"]) for i, k in th.iteritems()]
    ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
    buf += ''.join(
        {"Key Word: %-28s - Header Name: %-77s - Position: %-4s - Length: %i\n" %
         (header[0], get_header_human_name(header[0], th), str(get_header_position(header[0], th)),
          get_range(get_header_type(header[0], th))) for header in ordered_keys
         }
    )
    print buf
    return


def print_binary_header_key_word():
    """Print list of trace headers"""
    buf = "\nBinary header list ad key words\n\n"
    bh = copy.deepcopy(binary_header)
    header_pos = [(i, k["pos"]) for i, k in bh.iteritems()]
    ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
    buf += ''.join(
        {"Key Word: %-29s - Header Name: %-65s - Position: %-4s - Length: %i\n" %
         (header[0], get_header_human_name(header[0], bh), str(get_header_position(header[0], bh)),
          get_range(get_header_type(header[0], bh))) for header in ordered_keys
         }
    )
    print buf
    return


def create_ebcdic(string=None, file_input=None, ascii=False):
    """
    Creates EBCDIC header.
    From the arguments passed to the function, completes, cuts, converts them to ebcdic to return a 3200 bytes EBCDIC
    encoded string.

    Args:
        string (str): String of the textual header (must contained the carriage return)
        file_input (str): File name that contains the textual header

    Returns:
        str: 3200 bytes
    """
    split_ebcdic = ''
    if not string and not file_input:
        ebcdic_header = ''
        line1 = 'EBCDIC header Not defined'
        line2 = 'Target Energy Solutions Technologies %s' % str(datetime.now())
        ebcdic_header += line1 + ' ' * (EBCDIC_LINE_LENGTH - len(line1))
        ebcdic_header += line2 + ' ' * (EBCDIC_LINE_LENGTH - len(line2))
        ebcdic_header += ' ' * (EBCDIC_HEADER_LENGTH - 2 * EBCDIC_LINE_LENGTH)
        return ebcdic_header
    if string:
        split_ebcdic = string.splitlines()
    if file_input:
        if not os.path.exists(file_input):
            print "EBCID Header file not found: %s" % file_input
            exit()
        with open(file_input, 'r') as fin:
            split_ebcdic = fin.read().splitlines()
    ebcdic_header = ''
    if len(split_ebcdic) > EBCDIC_LINES:
        split_ebcdic = split_ebcdic[:EBCDIC_LINES]
    else:
        split_ebcdic.extend(' ' for i in range(0, EBCDIC_LINES - len(split_ebcdic)))
    for l in split_ebcdic:
        if len(l) > EBCDIC_LINE_LENGTH:
            ebcdic_header += l[:EBCDIC_LINE_LENGTH]
        else:
            ebcdic_header += l + ' ' * (EBCDIC_LINE_LENGTH - len(l))
    if not ascii:
        ebcdic_header = encode(ebcdic_header, "cp500")
    return ebcdic_header
