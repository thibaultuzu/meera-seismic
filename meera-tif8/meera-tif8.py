#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""MEERA TIF8 UTILITY"""
import functions
import click

_VERSION = "0.1"


@click.group()
@click.pass_context
@click.version_option(version=_VERSION)
def cmd(ctx):
    """Meera TIF8 Utility"""
    pass

cmd.add_command(functions.extract.cmd, 'extract')
cmd.add_command(functions.extract.cmd, 'convert')
cmd.add_command(functions.info.cmd, 'info')

if __name__ == '__main__':
    cmd(obj={})
