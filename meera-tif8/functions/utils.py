#!/usr/bin/env python
# utf-8
import os
import errno
import glob
# import time
import logging
log = logging.getLogger(__name__)
import socket
import ConfigParser
import re


def mkdir_p(path):
    try:
        log.debug("Creating directory %s", path)
        os.makedirs(path)
    except OSError, exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def quote(s):
    return s.replace("'", """'"'"'""")


def port_open(port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    r = s.connect_ex(('127.0.0.1', port))
    s.close()
    return r


def split_base(path, base_dir):
    path = os.path.abspath(path)
    parts = path.split(base_dir.strip())
    if len(parts) < 2:
        raise Exception("in split_base, path %s does not start with base_dir %s", path, base_dir)
    rel_path = parts[1]
    if rel_path.startswith('/'):
        rel_path = rel_path[1:]
    return rel_path


def transform_wkt(wkt, dim):
    if dim == '2D':
        wkt = re.sub("^LINESTRING * \(", "MULTILINESTRING((", wkt)
        wkt = re.sub("\) *LINESTRING *\(", "),(", wkt)
        wkt += ")"
    elif dim == '3D':
        wkt = re.sub("^POLYGON * \(", "MULTIPOLYGON((", wkt)
        wkt = re.sub("\) *POLYGON *\(", "),(", wkt)
        wkt += ")"
    return wkt


def walk_files(*paths):
    """walk_files walks through all the dirs and file in the `paths` list
    and return iterable of file paths. It also matches the glob operator

    Args:
        path (string): file/directory name

    Example:
    >>> 'utils.py' in walk_files('*.py')
    True
    >>> 'testdata/test.pdf' in walk_files('testdata')
    True
    >>> walk_files('edge.py').next()
    'edge.py'
    """
    for gp in paths:
        for p in glob.glob(gp):
            if os.path.isdir(p):
                for root, directory, filenames in os.walk(p):
                    for fn in filenames:
                        yield os.path.join(root, fn)
            else:
                yield p


def which(program):
    """
    Example:
    >>> which('ls')
    '/bin/ls'
    """
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None


def get_config(edge_config):
    """ get the edge load config

    >>> c = get_config("~/.edge.conf")
    >>> c.get('workflows', 'load')
    u'LoadFromMedia'
    >>> c.get('omni-well-docs-gen', 'main')
    u'ppdmFuseimWellDocument'
    """
    conf = ConfigParser.ConfigParser(allow_no_value=True)
    try:
        edge_config = os.path.expanduser(edge_config)
        if os.path.exists(edge_config):
            conf.read(edge_config)
        else:
            conf.read_string(DEFAULT_CONFIG)
        with open(edge_config, 'wb') as cf:
            conf.write(cf)
    except Exception, e:
        log.exception(e)
    return conf


if __name__ == "__main__":
    import doctest
    doctest.testmod()
