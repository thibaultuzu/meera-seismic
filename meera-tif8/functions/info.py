#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from tif8.TIF8 import TIF8
import click


@click.command(short_help="Display Summarized information of the TIF8 file")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-o', '--info_object', type=click.Choice(['tif8', 'segd']), default='tif8',
              help='Type of object you want to display info')
@click.option('-t', '--info_type', type=click.Choice(['summary', 'full', 'csv']), default='summary',
              help='Type of object you want to display info')
def cmd(file_pattern, info_object, info_type):
    buf_tif8 = buf_segd = ""
    if info_object == "tif8" and info_type == "csv":
        buf_tif8 += "file_name,block_number,offset,size,record_type\n"
    if info_object == "segd" and info_type == "csv":
        buf_segd += "file_name,ffs,offset,ff,sp,nb_traces,sample_rate,record_length,size,status,\n"

    for file in walk_files(file_pattern):
        # print file
        try:
            tif8 = TIF8(file)
            if info_object == "tif8":
                if info_type == "summary":
                    tif8.info_tif8()
                elif info_type == "full":
                    tif8.info_tif8_full()
                elif info_type == "csv":
                    buf_tif8 += tif8.info_tif8_csv(header=False, stdout=False)
            else:
                if info_type == "summary":
                    tif8.info_segd()
                elif info_type == "full":
                    tif8.info_segd_full()
                elif info_type == "csv":
                    buf_segd += tif8.info_segd_csv(header=False, stdout=False)
        except:
            "print TIF-8 error"
            continue
    if buf_tif8 != "":
        print buf_tif8
    if buf_segd != "":
        print buf_segd
