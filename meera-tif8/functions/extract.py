#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from tif8.TIF8 import TIF8
import click
import os


@click.command(short_help="Extract SEG-D files encapsulated in TIF8")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-k', '--key', type=click.Choice(['fs', 'ffid']), default="fs",
              help='Selection of the key for the extraction (fs: file sequential, ffid: Field File Id)')
@click.option('-i', '--items', type=str, required=True,
              help='Range of key value to extract (eg. 1..10,18,45..87); "all" will extract all the SEG-D files')
@click.option('-o', '--output_folder', type=str, help='Output folder (default: TIF8 folder). If output folder doesn\'t'
                                                      'exist it will be created')
@click.option('--file_name', type=click.Choice(['fs', 'ffid']), default="fs",
              help='if "fs" is selected the file name will reflect the sequential nb of the SEG-D file, else it will '
                   'reflect the Field File nb')
@click.option('--force_corrupted', is_flag=True, default=False,
              help='Write Corrupted SEG-D files')
def cmd(file_pattern, key, items, output_folder, file_name, force_corrupted):
    all_files = False
    if items == "all":
        list_range = None
        all_files = True
    else:
        rango = items.split(',')
        list_range = []
        for item in rango:
            if ".." in item:
                item0 = item.split("..")[0]
                item1 = item.split("..")[1]
                try:
                    i0 = int(item0)
                    i1 = int(item1)
                    list_range.append(range(i0, i1 + 1))
                except ValueError:
                    click.echo("Item list definition error")
                    exit()
            else:
                try:
                    i = int(item)
                    list_range.append(i)
                except ValueError:
                    click.echo("Item list definition error")
                    exit()

    if file_name == "fs":
        file_name_ffs = True
    else:
        file_name_ffs = False

    for file in walk_files(file_pattern):
        try:
            tif8 = TIF8(file)
            if not output_folder:
                output_folder = os.path.dirname(file)
            elif not os.path.isdir(output_folder):
                print "Output folder: %s doesn\'t exist it will be created" % output_folder
                os.makedirs(output_folder)

            if key == "fs":
                tif8.extract_segd_file(all_files=all_files, filename_ffs=file_name_ffs, dest_folder=output_folder,
                                       list_segd_nb=list_range, force_error=force_corrupted)
            else:
                if all_files:
                    tif8.extract_segd_file(all_files=all_files, filename_ffs=file_name_ffs, dest_folder=output_folder,
                                           list_segd_nb=list_range, force_error=force_corrupted)
                else:
                    for i in list_range:
                        if tif8.search_ff(i) == 0 or tif8.search_ff(i) == -1:
                            print "SEG-D File #%i not found in TIF8 file %s" % (i, file_name)
                        else:
                            tif8.extract_segd_file_by_ff(ff=i, filename_ffs=file_name_ffs, dest_folder=output_folder,
                                                         force_error=force_corrupted)
        except:
            "print TIF-8 error"
            continue
