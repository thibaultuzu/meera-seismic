#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from tif8.TIF8 import TIF8
import click
import os


@click.command(short_help="Convert SEG-D files to SEG-Y (valid for SEG-D 8015")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-k', '--key', type=click.Choice(['fs', 'ffid']), default="fs",
              help='Selection of the key for the conversion (fs: file sequential, ffid: Field File Id)')
@click.option('-i', '--items', type=str, required=True,
              help='Range of key value to extract (eg. 1..10,18,45..87); "all" will convert all the SEG-D files')
@click.option('-o', '--output_folder', type=str, help='Output folder (default: TIF8 folder). If output folder doesn\'t'
                                                      'exist it will be created')
@click.option('s', '--split', is_flag=True, default=False,
              help='Separate output files per SEG-D files')
@click.option('--output_file_name', type=str, default=None, help="Output file name. Does not apply when multiple keys "
                                                                 "selected and skip flag activated")
@click.option('--force_sercel_byte', is_flag=True, default=False,
              help='Force nb samples per trace +1 (set additional_byte option to modify the value of aditional bytes')
@click.option('--additional_byte', type=int, default=1,
              help='Additional samples to nb samples per trace')
@click.option('--ebcdic', type=click.Path(), help='EBCDIC file')
def cmd(file_pattern, key, items, output_folder, split, output_file_name, force_sercel_byte,
        additional_byte, force_corrupted, file_ebcdic):
    list_range = []
    if items != "all":
        rango = items.split(',')
        list_range = []
        for item in rango:
            if ".." in item:
                item0 = item.split("..")[0]
                item1 = item.split("..")[1]
                try:
                    i0 = int(item0)
                    i1 = int(item1)
                    list_range.append(range(i0, i1 + 1))
                except ValueError:
                    click.echo("Item list definition error")
                    exit()
            else:
                try:
                    i = int(item)
                    list_range.append(i)
                except ValueError:
                    click.echo("Item list definition error")
                    exit()

    if len(list_range) > 1 and split and output_file_name:
        click.echo("Output file name option does not apply when multiple keys selected and skip option triggered")
        exit()

    if file_ebcdic and not os.path.exists(file_ebcdic):
        click.echo("EBCDIC File does not exist")
        exit()

    if key == "fs":
        file_name_ffs = True
    else:
        file_name_ffs = False

    if force_sercel_byte and additional_byte:
        force_sercel_byte = additional_byte
    elif force_sercel_byte and not additional_byte:
        force_sercel_byte = 1

    for file in walk_files(file_pattern):
        try:
            tif8 = TIF8(file)
            if not output_folder:
                output_folder = os.path.dirname(file)
            elif not os.path.isdir(output_folder):
                click.echo("Output folder: %s doesn\'t exist it will be created" % output_folder)
                os.makedirs(output_folder)

            list_range_fs = []
            if key == "ffid":
                for i in list_range:
                    ffs = tif8.search_ff(i)
                    if tif8.search_ff(i) == 0 or tif8.search_ff(i) == -1:
                        click.echo("SEG-D File #%i not found in TIF8 file %s" % (i, file))
                    else:
                        list_range_fs.append(ffs)
            else:
                for i in list_range:
                    if i not in tif8.map_segd.iterkeys():
                        click.echo("File sequential #%i not in TIF8 file %s" % (i, file))
                    else:
                        list_range_fs.append(i)

            if not list_range_fs and items != "all":
                click.echo("Items selected not in TIF8 file")
                exit()

            if items == "all":
                list_range_fs = None

            tif8.convert_tif8_to_segy(split=split, file_name_ffs=file_name_ffs, list_segd_file=list_range_fs,
                                      output_folder=output_folder, force_sercel_byte=force_sercel_byte,
                                      file_ebcdic=file_ebcdic)
        except:
            continue


# def get_output_file_name(list_items, key, tif8_map, split, original_file):
#     if split:
#         if
#     return ''
#
