#!/usr/bin/env python
#coding:utf-8
"""Get help for sub commands

Usage: meera-tif8.py help [<command>]

Commands:
    info            Print information of TIF8 file and encapsulated files
    extract         Extract subset or all encapsulated files
    convert         Convert SEG-D files encapsulated to SEG-Y. Split or merge Output
"""
import click


@click.command()
@click.argument('argv')
def run(argv):
    from docopt import docopt
    import sys

    args = docopt(__doc__, argv=argv)
    cmd = args['<command>']

    if cmd is None:
        print __doc__
        sys.exit(1)
    elif cmd == 'info':
        import info
        print info.__doc__
    elif cmd == 'extract':
        import extract
        print extract.__doc__
    elif cmd == 'convert':
        import convert
        print convert.__doc__
