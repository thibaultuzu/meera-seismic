#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from segy.segy import Segy, SegyFormatError
import click


@click.command(short_help="Display Summarized information of the segy file")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
def cmd(file_pattern):
    for file in walk_files(file_pattern):
        # print file
        try:
            sgy = Segy(file)
            sgy.info()
        except SegyFormatError:
            continue

