#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from segy.segy import Segy, SegyFormatError
from segy.segy_utils import print_trace_header_key_word
import click
import os
import copy
from segy.constants_sgy import trace_header as th_ref, binary_header as bh_ref


@click.command()
@click.argument('-f', '--file_pattern', type=click.Path(),  help='Files or folder to merge')
@click.option('-e', '--ebcdic_file', type=click.Path(),
              help='File location of EBCDIC header content. Valid when \'ebcdic_header\' header is selected')
@click.option('-a', '--ascii', is_flag=True,
              help='EBCDIC Header will be coded in ASCII. Valid when \'ebcdic_header\' header is selected')
def cmd(file_pattern, ebcdic_file, ascii):
    """Edit trace header, binary header or EBCDIC header. With constant or sequential values in case of trace
    header and binary header and with text enccoded in Ascii or EBDCID for EBCDIC header"""

    if header == 'ebcdic_header' and not ebcdic_file:
        click.echo("You must select a file location (ebcdic_file option) to edit the EBCDIC header")
    if (header == 'trace_header' or header == 'binary_header') and not value:
        click.echo("You must select a value to assign to the selected header")
    if (header == 'trace_header' or header == 'binary_header') and not header_key_word:
        click.echo("You must select a header_key_word to assign to the selected header")
    if (header == 'trace_header' or header == 'binary_header') and ascii:
        click.echo("ascii flag doesn't apply to this kind of header")
    if header == 'ebcdic_header' and value:
        click.echo("Value does not apply the EBCDIC header")
    if header == 'ebcdic_header' and header_key_word:
        click.echo("header_key_word value does not apply the EBCDIC header")
    if header == 'ebcdic_header' and sequential:
        click.echo("Sequential option does not apply the EBCDIC header")

    for file in walk_files(file_pattern):
        try:
            sgy = Segy(file)
            if header == 'ebcdic_header':
                        sgy.edit_ebcdic(file_input=ebcdic_file, ascii=ascii)

            elif header == 'binary_header':
                bh = copy.deepcopy(bh_ref)
                if header_key_word not in bh.keys():
                    click.echo("Binary header key word not in binary header keywords")
                    print_binary_header_key_word()
                else:
                    sgy.edit_binary(header_key_word, value)
            elif header == 'trace_header':
                th = copy.deepcopy(th_ref)
                if header_key_word not in th.keys():
                    click.echo("Trace header key word not in trace header keywords")
                    print_trace_header_key_word()
                else:
                    sgy.edit_trace_header(header_key_word, value, sequential, sequential_increment)
        except SegyFormatError:
            continue
