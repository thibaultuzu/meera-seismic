#! /usr/bin/env python
# -*- coding: utf-8 -*-
from segy.segy_utils import print_trace_header_key_word, print_binary_header_key_word
import click


@click.command(short_help="Print list of binary headers trace headers and key words")
def cmd():
    print_binary_header_key_word()
    print_trace_header_key_word()
