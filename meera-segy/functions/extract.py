#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from segy.segy import Segy, SegyFormatError
from segy.segy_utils import print_trace_header_key_word
import click
import copy
from segy.constants_sgy import trace_header


@click.command(short_help="Extract subset of segy file defined by list of key words value (eg. sp from 1 to 100)")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-k', '--key', default="tr_nb", required=True,
              help='Trace Header key word (use list_headers command for complete list)')
@click.option('-i', '--items', type=str, required=True, help='Range of key value to extract (eg. 1..10,18,45...87)')
@click.option('-o', '--output', type=str, help='Output file (default: input_file + key_word + .extract.sgy")')
def cmd(file_pattern, key, items, output):
    key = key.encode('utf-8')
    th = copy.deepcopy(trace_header)
    th["tr_nb"] = {"pos": "N/A", "type": "N2", "human_name": "Trace number within file (not read from trace header)"}
    if key not in th.keys():
        click.echo("Key word not in list_headers key")
        print_trace_header_key_word()
        exit()

    rango = items.split(',')
    list_range = []
    for item in rango:
        if ".." in item:
            item0 = item.split("..")[0]
            item1 = item.split("..")[1]
            try:
                i0 = int(item0)
                i1 = int(item1)
                list_range.append(range(i0, i1+1))
            except ValueError:
                click.echo("Range definition error")
                exit()
        else:
            try:
                i = int(item)
                list_range.append(i)
            except ValueError:
                click.echo("Range definition error")
                exit()

    for file in walk_files(file_pattern):
        # print file
        try:
            sgy = Segy(file)
            if not output:
                output_file = file + ".extract.%s.sgy" % key
            else:
                output_file = output
            sgy.polycut(header=key, list_to_extract=list_range, output_file=output_file)
        except SegyFormatError:
            continue
