#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from segy.segy import Segy, SegyFormatError
import click


@click.command(short_help="Spatial Polycut segy with wkt polygon input")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-w', '--wkt', type=str, required=True, help='Wkt string')
@click.option('--crs_id_segy', default=4326, help='CRS id of the coordinates in SEG-Y trace headers')
@click.option('--crs_id_wkt', default=4326, help='CRS id of the coordinates in wkt string')
@click.option('--pos_x', default=180, help='Byte position of the X coordinates in trace header')
@click.option('--pos_y', default=184, help='Byte position of the Y coordinates in trace header')
@click.option('--length_x', default=4, help='Length in bytes of the X coordinate trace header')
@click.option('--length_y', default=4, help='Length in bytes of the Y coordinate trace header')
@click.option('--coord_multiplier', default=1,
              help='Additional scalar to apply to coordinates (added to the one read in trace header byte 71)')
@click.option('-o', '--output', type=str, help='Output file (default: input_file + .spatial_extract.sgy")')
def cmd(file_pattern, wkt, crs_id_segy, crs_id_wkt, pos_x, pos_y, length_x, length_y, coord_multiplier, output):
    for file in walk_files(file_pattern):
        try:
            sgy = Segy(file)
            if not output:
                output_file = file + ".spatial_extract.sgy"
            else:
                output_file = output
            sgy.spatial_polycut(wkt=wkt, crsIdSegy=crs_id_segy, crsIdPolygon=crs_id_wkt, posX=pos_x, posY=pos_y,
                                lengthX=length_x, lengthY=length_y, output_file=output_file,
                                coordMultiplier=coord_multiplier)
        except SegyFormatError:
            continue
