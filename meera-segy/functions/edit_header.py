#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from segy.segy import Segy, SegyFormatError
from segy.segy_utils import print_trace_header_key_word, print_binary_header_key_word
import click
import copy
import os
from segy.constants_sgy import trace_header as th_ref, binary_header as bh_ref


@click.command(short_help="Edit trace header, binary header or EBCDIC header. With constant or sequential values "
                          "in case of trace header and binary header and with text enccoded in Ascii or EBDCID for "
                          "EBCDIC header")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-h', '--header', type=click.Choice(['ebcdic_header', 'binary_header', 'trace_header']),
              help='Header Block to edit (\'ebcdic_header\',\'binary_header\', \'trace_header\')', required=True, )
@click.option('-k', '--header_key_word', type=str,
              help='Selection of comma separated headers key word (use list_headers command for '
                   'complete list) to display. Valid when \'binary_header\' and \'trace_header\' headers are selected')
@click.option('-s', '--sequential', is_flag=True,
              help='Set the trace header value sequentially starting from the \'value\' option. Valid when '
                   '\'trace_header\' header is selected')
@click.option('-i', '--sequential_increment', type=int, default=1,
              help='Set up Integer incremental value can be negative. Valid when sequential option is selected'
                   '\'trace_header\' header is selected')
@click.option('-v', '--value', type=int,
              help='Header Value. Value set to first trace in case the sequential option is set up. Valid when '
                   '\'binary_header\' and \'trace_header\' headers are selected')
@click.option('-e', '--ebcdic_file', type=click.Path(),
              help='File location of EBCDIC header content. Valid when \'ebcdic_header\' header is selected')
@click.option('-a', '--ascii', is_flag=True,
              help='EBCDIC Header will be coded in ASCII. Valid when \'ebcdic_header\' header is selected')
def cmd(file_pattern, header, header_key_word, sequential, sequential_increment, value, ebcdic_file, ascii):
    status = 0
    if header == 'ebcdic_header' and not ebcdic_file:
        click.echo("You must select a file location (ebcdic_file option) to edit the EBCDIC header")
        status = 1
    if (header == 'trace_header' or header == 'binary_header') and not value:
        click.echo("You must select a value to assign to the selected header")
        status = 1
    if (header == 'trace_header' or header == 'binary_header') and not header_key_word:
        click.echo("You must select a header_key_word to assign to the selected header")
        status = 1
    if (header == 'trace_header' or header == 'binary_header') and ascii:
        click.echo("ascii flag doesn't apply to this kind of header")
        status = 1
    if header == 'ebcdic_header' and value:
        click.echo("Value does not apply to EBCDIC header")
        status = 1
    if header == 'ebcdic_header' and header_key_word:
        click.echo("header_key_word value does not apply to EBCDIC header")
        status = 1
    if (header == 'ebcdic_header' or header == 'binary_header') and sequential:
        click.echo("Sequential option does not apply the EBCDIC header")
        status = 1
    if header == 'ebcdic' and not os.path.exists(ebcdic_file):
        click.echo("EBCDIC File does not exist")
        status = 1
    if status > 0:
        exit()

    # Adjust value to handle the first trace = value + 1
    if header == 'trace_header' and value:
        value -= 1

    for file in walk_files(file_pattern):
        try:
            sgy = Segy(file)
            if header == 'ebcdic_header':
                sgy.edit_ebcdic(file_input=ebcdic_file, ascii=ascii)

            elif header == 'binary_header':
                bh = copy.deepcopy(bh_ref)
                if header_key_word not in bh.keys():
                    click.echo("Binary header key word not in binary header keywords")
                    print_binary_header_key_word()
                else:
                    sgy.edit_binary(header_key_word, value)
            elif header == 'trace_header':
                th = copy.deepcopy(th_ref)
                if header_key_word not in th.keys():
                    click.echo("Trace header key word not in trace header keywords")
                    print_trace_header_key_word()
                else:
                    sgy.edit_trace_header(header_key_word, value, sequential, sequential_increment)
        except SegyFormatError:
            continue
