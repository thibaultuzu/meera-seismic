#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files, get_list_range
from segy.segy import Segy, SegyFormatError
from segy.segy_utils import print_trace_header_key_word
import click
import copy
from segy.constants_sgy import trace_header as th_ref


@click.command(short_help="Output a dump of a list (or all) trace headers (Option: -l) for a given list (or all) "
                          "of traces (Option: -l) in csv format")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-t', '--traces', type=str, default="all",
              help='Selection of trace number which trace headers will be displayed (eg. 1..10,18,45...87); '
                   '"all" will display the trace headers selected for all the traces')
@click.option('-l', '--trace_header', type=str, default="all", required=True,
              help='Selection of comma separated Trace Header key word (eg. ensemble_number,shot_point_number '
                   '(use list_headers command for complete list) to display. "all" will output all trace headers')
@click.option('-o', '--output_file', type=click.Path(), default=None, help='Output file. If no path, output is stdout')
def cmd(file_pattern, traces, trace_header, output_file):
    th = copy.deepcopy(th_ref)
    th["tr_nb"] = {"pos": "N/A", "type": "N2", "human_name": "Trace number within file (not read from trace header)"}

    if trace_header == "all":
        trace_header = ["all"]
    else:
        trace_header = trace_header.split(',')
        for t in trace_header:
            if t not in th.keys():
                click.echo("Trace header key word not in trace headers keywords")
                print_trace_header_key_word()
                exit()

    if traces == "all":
        list_range = None
    else:
        list_range = get_list_range(traces)
        if list_range == 0:
            click.echo("Traces definition error")
            exit()

    for file in walk_files(file_pattern):
        # print file
        try:
            sgy = Segy(file)
            sgy.trace_header(list_range, trace_header, output_file)
        except SegyFormatError:
            continue
