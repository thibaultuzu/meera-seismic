#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
import click
from segy.segy import Segy, SegyFormatError


@click.command(short_help="Display Binary Header information")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
def cmd(file_pattern):
    for file in walk_files(file_pattern):
        # print file
        try:
            sgy = Segy(file)
            click.echo("\n\nSEG-Y File: %s\n" % file)
            sgy.print_binary()
        except SegyFormatError:
            continue
