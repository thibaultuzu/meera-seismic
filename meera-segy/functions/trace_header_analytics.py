#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files
from segy.segy import Segy, SegyFormatError
from segy.segy_utils import print_trace_header_key_word
import click
import copy
from segy.constants_sgy import trace_header as th_ref


@click.command(short_help="Display min,max,mean,average for given trace_headers")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('-l', '--trace_header', type=str, default="all", required=True,
              help='Selection of comma separated Trace Header key word (eg. ensemble_number,shot_point_number '
                   '(use list_headers command for complete list) to display. "all" will output all trace headers')
def cmd(file_pattern, trace_header):
    th = copy.deepcopy(th_ref)
    th["tr_nb"] = {"pos": "N/A", "type": "N2", "human_name": "Trace number within file (not read from trace header)"}

    if trace_header == "all":
        trace_header = ["all"]
    else:
        trace_header = trace_header.split(',')
        for t in trace_header:
            if t not in th.keys():
                click.echo("Trace header key word not in trace headers keywords")
                print_trace_header_key_word()
                exit()

    print "file_name,header,min,max,median,average"
    for file in walk_files(file_pattern):
        # print file
        try:
            sgy = Segy(file)
            # click.echo("\n\nSEG-Y File: %s\n" % file)
            # print "header,min,max,median,average"
            for h in trace_header:
                minH = sgy.header_min(header=h)
                maxH = sgy.header_max(header=h)
                medianH = sgy.header_median(header=h)
                meanH = sgy.header_mean(header=h)
                print "%s,%s,%i,%i,%i,%i" % (file, h, minH, maxH, medianH, meanH)
        except SegyFormatError:
            continue
