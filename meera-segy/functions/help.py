#!/usr/bin/env python
#coding:utf-8
"""Get help for sub commands

Usage: meera-segy.py help [<command>]

Commands:
    ebcdic          Print EBCDIC header of input files
    info            Print General Information of input files
    binary          Print Binary header of input files
    extract         Create subset of segy files
    list_headers    Print list trace header key word to create subset
"""
import click


@click.command()
@click.argument('argv')
def run(argv):
    from docopt import docopt
    import sys

    args = docopt(__doc__, argv=argv)
    cmd = args['<command>']

    if cmd is None:
        print __doc__
        sys.exit(1)
    elif cmd == 'ebcdic':
        import ebcdic
        print ebcdic.__doc__
    elif cmd == 'info':
        import info
        print info.__doc__
    elif cmd == 'binary':
        import binary
        print binary.__doc__
    elif cmd == 'extract':
        import extract
        print extract.__doc__
    elif cmd == 'list_headers':
        import list_headers
        print list_headers.__doc__
