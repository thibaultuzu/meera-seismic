#! /usr/bin/env python
# -*- coding: utf-8 -*-
from utils import walk_files, transform_wkt
from segy.segy import Segy, SegyFormatError
import click
import re


@click.command(short_help="Extract WKT of the envelope of the SEG-Y file (dim='3D': POLYGON; dim='2D'=LINESTRING)")
@click.option('-f', '--file_pattern', type=click.Path(), required=True, help='File or folder input')
@click.option('--crs_id_output', default=4326, help='CRS id of WKT coordinates')
@click.option('--crs_id_segy', default=4326, help='CRS id of the coordinates in SEG-Y trace headers')
@click.option('--pos_x', default=180, help='Byte position of the X coordinates in trace header')
@click.option('--pos_y', default=184, help='Byte position of the Y coordinates in trace header')
@click.option('--length_x', default=4, help='Length in bytes of the X coordinate trace header')
@click.option('--length_y', default=4, help='Length in bytes of the Y coordinate trace header')
@click.option('--dim', type=click.Choice(['2D', '3D']), default='3D', help='Dimension of the SEG-Y file')
@click.option('--coord_multiplier', default=1,
              help='Additional scalar to apply to coordinates (added to the one read in trace header byte 71)')
@click.option('--multi', is_flag=True, default=False,
              help='Gather the wkt extracted in a multi geometry (MULTILINE, MULTIPOLYGON)')
def cmd(file_pattern, crs_id_output, crs_id_segy, pos_x, pos_y, length_x, length_y, coord_multiplier, dim, multi):
    """Extract WKT of the convex hull envelope of the SEG-Y file"""
    wkt_multi = ''
    for file in walk_files(file_pattern):
        # print file
        try:
            sgy = Segy(file)
            click.echo("\n\nWKT - File: %s\n" % file)
            wkt = sgy.wkt_outline(crs_id_output, crs_id_segy, pos_x, pos_y, length_x, length_y, coord_multiplier, dim)
            if multi:
                wkt_multi += wkt.wkt
        except SegyFormatError:
            continue
    if multi and wkt_multi:
        wkt_multi = transform_wkt(wkt_multi, dim)
        click.echo("\n\nWKT - Multi: \n\n%s" % wkt_multi)
