#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""MEERA SEG-Y UTILITY"""
import click

import functions

_VERSION = "0.1"


@click.group()
@click.pass_context
@click.version_option(version=_VERSION)
def cmd(ctx):
    """Meera SEG-Y Utility"""
    pass

cmd.add_command(functions.binary.cmd, 'binary')
cmd.add_command(functions.ebcdic.cmd, 'ebcdic')
cmd.add_command(functions.extract.cmd, 'extract')
cmd.add_command(functions.spatial_extract.cmd, 'spatial_extract')
cmd.add_command(functions.help.run, 'help')
cmd.add_command(functions.list_headers.cmd, 'list_headers')
cmd.add_command(functions.wkt.cmd, 'wkt')
cmd.add_command(functions.info.cmd, 'info')
cmd.add_command(functions.trace_header.cmd, 'trace_header')
cmd.add_command(functions.edit_header.cmd, 'edit_header')
cmd.add_command(functions.trace_header_analytics.cmd, 'trace_header_analytics')

if __name__ == '__main__':
    cmd(obj={})
