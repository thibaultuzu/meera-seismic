from struct import unpack as structUnpack, pack as structPack
from bitstring import Bits
from binascii import hexlify


def unpack(data, type, big_endian=True):
    if big_endian:
        endian = '>'
    else:
        endian = '<'
    if type[0] == 'H' and type[1] == '+':
        return hexlify(data)[:-1]
    elif type[0] == 'H' and type[1] == '-':
        return hexlify(data)[1:]
    elif type[0] == 'H' and type[1] == '3':
        return hexlify("\x00" + data)
    elif type[0] == 'H' and int(type[1]) in range(9):
        return hexlify(data)
    elif type[0] == 'n' and int(type[1]) in range(9):
        return structUnpack(endian + 'h', data)[0]
    elif type[0] == 'N' and int(type[1]) == 4:  # At the moment just N3
        return structUnpack(endian + 'i', data)[0]
    elif type[0] == 'N' and int(type[1]) == 2:  # At the moment just N3
        return structUnpack(endian + 'h', data)[0]
    elif type[0] == 'N' and int(type[1]) == 3:  # At the moment just N3
        return structUnpack(endian + 'i', '\x00' + data)[0]
    elif type[0] == 'B' and type[1] == '+':
        return Bits(bytes=data).bin[:-4]
    elif type[0] == 'B' and type[1] == '-':
        return Bits(bytes=data).bin[4:]
    elif type[0] == 'B' and int(type[1]) in range(9):
        return Bits(bytes=data).bin
    elif type[0] == 'S' and int(type[1]) in range(9):  # Revision SEG-D
        return str(int(hexlify(data[0]))) + '.' + str(int(hexlify(data[1])))
    elif type[0] == 'E':  # Sample exponent
        return 2 ** int(hexlify(data)[0])
    elif type[0] == 's':  # String
        return hexlify(data).decode("hex")
        # return unhexlify(data)
        # return b2a_hex(data)
        # return bytes.fromhex(data)


def pack(data, type, big_endian=True):
    if big_endian:
        endian = '>'
    else:
        endian = '<'
    if type[0] == 'N' and int(type[1:]) == 2:
        return structPack(endian + 'h', data)
    if type[0] == 'N' and int(type[1:]) == 4:
        return structPack(endian + 'i', data)
    if type[0] == 'I' and int(type[1:]) == 2:
        return structPack(endian + 'H', data)
    if type[0] == 'I' and int(type[1:]) == 4:
        return structPack(endian + 'I', data)
    if type[0] == 'Z':  # Case unassigned gaps in binary header, returns bytestream of zero
        return int(type[1:]) * '\x00'
    if type[0] == 'S' and int(type[1:]) == 2:
        return structPack(endian + 'H', data)
    if type[0] == 'R' and int(type[1:]) == 2:  # TODO: review segy rev packing
        return structPack('<H', data)
