from pack_unpack import pack
from codecs import encode
from constants_sgy import binary_header, trace_header
from datetime import datetime
import numpy as np
from struct import unpack as structUnpack, pack as structPack
from binascii import hexlify
from ieee2ibm import ieee2ibm as conv

EBCDIC_HEADER_LENGTH = 3200
EBCDIC_LINE_LENGTH = 80
EBCDIC_LINES = 40


def ebcdic(string=None, file_input=None, ascii=False):
    """
    Creates EBCDIC header.
    From the arguments passed to the function, completes, cuts, converts them to ebcdic to return a 3200 bytes EBCDIC
    encoded string.

    Args:
        string (str): String of the textual header (must contained the carriage return)
        file_input (str): File name that contains the textual header

    Returns:
        str: 3200 bytes
    """
    if not string and not file_input:
        ebcdic_header = ''
        line1 = 'EBCDIC header Not defined'
        line2 = 'Meera Technologies %s' % str(datetime.now())
        ebcdic_header += line1 + ' ' * (EBCDIC_LINE_LENGTH - len(line1))
        ebcdic_header += line2 + ' ' * (EBCDIC_LINE_LENGTH - len(line2))
        ebcdic_header += ' ' * (EBCDIC_HEADER_LENGTH - 2 * EBCDIC_LINE_LENGTH)
        return ebcdic_header
    if string:
        split_ebcdic = string.splitlines()
    if file_input:
        with open(file_input, 'r') as fin:
            split_ebcdic = fin.read().splitlines()
    ebcdic_header = ''
    if len(split_ebcdic) > EBCDIC_LINES:
        split_ebcdic = split_ebcdic[:EBCDIC_LINES]
    else:
        split_ebcdic.extend(' ' for i in range(0, EBCDIC_LINES - len(split_ebcdic)))
    for l in split_ebcdic:
        if len(l) > EBCDIC_LINE_LENGTH:
            ebcdic_header += l[:EBCDIC_LINE_LENGTH]
        else:
            ebcdic_header += l + ' ' * (EBCDIC_LINE_LENGTH - len(l))
    if not ascii:
        ebcdic_header = encode(ebcdic_header, "cp500")
    return ebcdic_header


def binary(binary_dict):
    """
    Create the binary header block.
    Mandatory fields are recovered from the dictionnary passed as argument, if they don't exist an error appears.
    If the input dictionnary has no value for a given header the default value is set.
    Values extracted from the input dictionnary and/or default values are packed according to their type

    Args:
        binary_dict (dict):
        key = header_name (str). Must be parts of the standard headers described in the binary_header dictionnary
        (see constants_sgy.py)
        value = raw value that will be packed through pack method according to their standard type described in the
        binary_header dictionnary (see constants_sgy.py)

    Returns:
        binary string: Binary header (400 bytes)
    """
    binary_h = str()
    header_pos = [(i, k["pos"]) for i, k in binary_header.iteritems()]
    ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
    for header in ordered_keys:
        if "def" in binary_header[header[0]] and header[0] not in binary_dict:
            binary_h += pack(binary_header[header[0]]["def"], binary_header[header[0]]["type"])
        elif header[0] in binary_dict:
            binary_h += pack(binary_dict[header[0]], binary_header[header[0]]["type"])
        elif "def" not in binary_header[header[0]] and header[0] not in binary_dict:
            print "Binary header: %s has no value attributed" % header[0]
    return binary_h


def th(th_dict):
    """
    Create the trace header 240 bytes string.
    Mandatory fields are recovered from the dictionnary passed as argument, if they don't exist an error appears.
    If the input dictionnary has no value for a given header the default value is set.
    Values extracted from the input dictionnary and/or default values are packed according to their type

    Args:
        th_dict (dict):
        key = header_name (str). Must be parts of the standard headers described in the trace_header dictionnary
        (see constants_sgy.py)
        value = raw value that will be packed through pack method according to their standard type described in the
        trace_header dictionnary (see constants_sgy.py)

    Returns:
        binary string: Trace header
    """
    tr_header = str()
    header_pos = [(i, k["pos"]) for i, k in trace_header.iteritems()]
    ordered_keys = sorted(header_pos, key=lambda pos: pos[1])
    for header in ordered_keys:
        if "def" in trace_header[header[0]] and header[0] not in th_dict:
            tr_header += pack(trace_header[header[0]]["def"], trace_header[header[0]]["type"])
        elif header[0] in th_dict:
            tr_header += pack(th_dict[header[0]], trace_header[header[0]]["type"])
        elif "def" not in trace_header[header[0]] and header[0] not in th_dict:
            print "Trace header: %s has no value attributed" % header[0]
    return tr_header


def ieee2ibm(ieee):
    """
    Convert a IEEE floating point number into IBM floating point format
    From Robert Kent (http://osdir.com/ml/python-scientific-user/2011-06/msg00084.html)
    """
    ieee = ieee.astype(np.float32)
    expmask = 0x7f800000
    signmask = 0x80000000
    mantmask = 0x7fffff
    asint = ieee.view('i4')
    signbit = asint & signmask
    exponent = ((asint & expmask) >> 23) - 127
    # The IBM 7-bit exponent is to the base 16 and the mantissa is presumed to
    # be entirely to the right of the radix point. In contrast, the IEEE
    # exponent is to the base 2 and there is an assumed 1-bit to the left of the
    # radix point.
    exp16 = ((exponent + 1) // 4)
    exp_remainder = (exponent + 1) % 4
    exp16 += exp_remainder != 0
    downshift = np.where(exp_remainder, 4 - exp_remainder, 0)
    ibm_exponent = np.clip(exp16 + 64, 0, 127)
    expbits = ibm_exponent << 24
    # Add the implicit initial 1-bit to the 23-bit IEEE mantissa to get the
    # 24-bit IBM mantissa. Downshift it by the remainder from the exponent's
    # division by 4. It is allowed to have up to 3 leading 0s.
    ibm_mantissa = ((asint & mantmask) | 0x800000) >> downshift
    # Special-case 0.0
    ibm_mantissa = np.where(ieee, ibm_mantissa, 0)
    expbits = np.where(ieee, expbits, 0)
    return signbit | expbits | ibm_mantissa


def trace_convert(data, tr_nb, from_fmt, to_fmt, big_endian=True):
    if from_fmt == "8015":
        tr_data = str()
        if len(data) % 10 != 0:
            print "Sample number issue tr_nb %i - trace length not multiple of 10" % tr_nb
            return
        group_samples_tr = len(data) / 10
        for i in range(0, group_samples_tr):
            _4samples = data[i * 10:i * 10 + 10]
            samples = sample_8015_to_float(_4samples)
            for sample in samples:
                tr_data += structPack('>f', sample)
            i += 1
        return tr_data
    else:
        return data


def sample_8015_to_float(sample):
    unpacked = structUnpack('>2B4h', sample)

    exponents = []
    exponents_raw = unpacked[0:2]
    fractions = unpacked[2:6]

    exponents.append(exponents_raw[0] >> 4)
    exponents.append(exponents_raw[0] & 15)
    exponents.append(exponents_raw[1] >> 4)
    exponents.append(exponents_raw[1] & 15)

    floats = ()
    for i in range(0, 4):
        fraction = fractions[i]
        if fraction < 0 and exponents[i] == 0:
            fraction = -(~fraction)
        floats += (fraction * 2 ** (exponents[i] - 15), )

    return floats

if __name__ == '__main__':
    # with open('/home/thibaultuzu/PycharmProjects/segd/samples/segd_cuba/00001052.segd', 'rb') as f:
    #     f.seek(22356)
    #     data = f.read(4)
    # ieee = unp('>f', data)[0]
    # print p('>f', ieee)
    # print ieee
    # ibm = conv(0)
    # ibm = ieee2ibm(np.float32(ieee))
    # ibm = ieee2ibm(np.array([0b01001001010110110111001011110001]))

    # print ibm
    # print hexlify(f.read(4))

    with open('/home/thibaultuzu/Desktop/samples/samples_OGDR/2D_FIELDSEGD/extract/1566.segd', 'rb') as f:
        f.seek(4404)
        s = f.read(10)

        sample_8015_to_float(s)

conversion = {
    "8015": sample_8015_to_float
}


