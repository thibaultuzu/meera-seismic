import os
from TIF8 import TIF8
from humanize import naturalsize
from progressbar import ProgressBar, Bar, Percentage, ETA
import shutil


class ExportSeismic:
    def __init__(self, list_input, output_directory, request_name, offline=False, progressBar=True):
        self.list_input = list_input
        self.connectivity = offline
        self.progressBar = progressBar
        self.output_directory = output_directory
        self.request_name = request_name
        self.export_map = self._get_export_map()
        self.size = self._get_export_size()
        self.line_list = self._get_line_list()

    def _get_export_size(self):
        size_total = 0
        for k in self.export_map:
            size_total += self.export_map[k]["size"]
        return size_total

    def _get_export_size_format(self, format):
        size_total = 0
        for k in self.export_map:
            if self.export_map[k]["format_export"] == format:
                size_total += self.export_map[k]["size"]
        return size_total

    #
    # def _get_export_size_line(self):
    #     size_total = 0
    #     for k in self.export_map:
    #         if self.export_map[k]["format_export"] == format:
    #             size_total += self.export_map[k]["size"]
    #     return size_total

    def _get_qty_files(self, format):
        if format != 'segd':
            files_set = set()
            for k in self.export_map:
                if self.export_map[k]["format"] == format:
                    files_set.add(self.export_map[k]["file_name"])
            return len(files_set)
        else:
            segd_qty = 0
            for k in self.export_map:
                if self.export_map[k]["format_export"] == "segd":
                    segd_qty += 1
            return segd_qty

    @staticmethod
    def file_len(file_name):
        with open(file_name) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

    def _get_line_list(self):
        line_list = set()
        for k in self.export_map.keys():
            line_list.add(self.export_map[k]["line_name"])
        return line_list

    def _get_export_map(self):

        export_map = {}
        tif8 = None
        line_map = {}
        size = 0
        fin = open(list_input)
        lines_qty = self.file_len(list_input)
        if self.progressBar:
            pbar = ProgressBar(maxval=lines_qty,
                               widgets=['Scanning Seismic Request ', Bar('#', '[', ']'), ' ', Percentage(), ' ', ETA()])
            pbar.start()
        l = 0
        for line in fin:
            l += 1
            if self.progressBar:
                pbar.update(l)
            line = line.split(',')

            file_name = line[0]
            file_name = file_name.replace('/seismic/seismic/', '/data/seismic/')
            if not os.path.exists(file_name):
                print "File: %s not found" % file_name
                continue

            fmt = file_name.split('.')[-1].lower()
            if fmt in ['seg-y', 'sgy', 'segy', 'sg-y']:
                fmt = 'sgy'
                fmt_export = 'sgy'
            elif fmt in ['tif8', 'tif-8']:
                fmt = 'tif8'
                fmt_export = 'segd'
            else:
                continue

            line_name = line[1]
            ffid0 = line[6]
            ffid1 = line[7]
            ffid_range = self._get_ff_range(ffid0, ffid1)

            if not ffid_range:
                continue

            for ffid in ffid_range:
                # At the moment for sgy we work on a group by line_name/sgy_file
                if fmt == 'sgy':
                    map_key = file_name + '_' + line_name
                else:
                    map_key = file_name + '_' + line_name + '_' + str(ffid)

                if map_key in export_map and fmt == 'tif8':
                    print "File: %s - Line: %s - FF: %i - duplicated" % (file_name, line_name, ffid)
                    continue

                if fmt == 'sgy':
                    size = os.path.getsize(file_name)
                elif fmt == 'tif8':
                    if not tif8 or tif8.filename != file_name:
                        if tif8:
                            tif8.close()
                        try:
                            tif8 = TIF8(file_name, dictionary_input=True,
                                        dictionary_output=True, progressBar=False)
                        except:
                            print "File: %s - FF: %i - TIF8 format corrupted" % (file_name, ffid)
                            continue
                    try:
                        # print "File: %s - FF: %i" % (file_name, ffid)
                        ffs = tif8.search_ff(ffid)
                        size = tif8.size_segd(ffid)
                    except:
                        print "File: %s - FF: %i - Not able to get the size" % (file_name, ffid)
                        continue
                    if ffs == -1:
                        print "File: %s - FF: %i - duplicated in TIF8" % (file_name, ffid)
                        continue
                    if ffs == 0:
                        print "File: %s - FF: %i - not found" % (file_name, ffid)
                        continue
                    if tif8.map_segd[ffs]["status"] == 0:
                        print "File: %s - FF: %i, SEG-D file truncated not included in the request" % (file_name, ffid)
                        continue

                if map_key not in export_map:
                    export_map[map_key] = {}
                    if fmt == 'sgy':
                        export_map[map_key]["ffids"] = set([])
                export_map[map_key]["size"] = size
                export_map[map_key]["format"] = fmt
                export_map[map_key]["format_export"] = fmt_export
                export_map[map_key]["file_name"] = file_name
                export_map[map_key]["line_name"] = line_name
                if export_map[map_key]["format"] == 'sgy':
                    export_map[map_key]["ffids"].add(ffid)
                if export_map[map_key]["format"] == 'tif8':
                    export_map[map_key]["ffid"] = ffid

                if line_name + '_' + fmt in line_map:
                    line_map[line_name + '_' + fmt]["ffids"].add(ffid)
                else:
                    line_map[line_name + '_' + fmt] = {}
                    line_map[line_name + '_' + fmt]["format"] = fmt
                    line_map[line_name + '_' + fmt]["line_name"] = line_name
                    line_map[line_name + '_' + fmt]["ffids"] = set([ffid])

        fin.close()
        if self.progressBar:
            pbar.finish()
        self._get_output_name(export_map, line_map)
        return export_map

        # Calculate file name with leading zero
        # output_file_name = os.path.join(output_directory, request_name,
        # "Field Data", line_name, "SEGY", "FF_")

    def _get_output_name(self, export_map, line_map):
        for l in line_map:
            fmt = line_map[l]["format"]
            line_name = line_map[l]["line_name"]
            max_ffid = max(line_map[l]["ffids"])
            size_ffid = len(str(max_ffid))

            for k in export_map:
                if export_map[k]["line_name"] == line_name and export_map[k]["format"] == fmt:
                    ext = export_map[k]["format_export"]
                    if ext == 'sgy':
                        folder_data_type = 'SEG-Y'
                        file_name = "FF_%s_%s.%s" % (str(min(export_map[k]["ffids"])).zfill(size_ffid),
                                                     str(max(export_map[k]["ffids"])).zfill(size_ffid), ext)
                    elif ext == 'segd':
                        ffid_file_name = str(export_map[k]["ffid"]).zfill(size_ffid)
                        folder_data_type = 'SEG-D'
                        file_name = "FF_%s.%s" % (ffid_file_name, ext)

                    directory_path = os.path.join(self.output_directory, self.request_name, "Field Data",
                                                  "LINE_%s" % line_name, folder_data_type)
                    export_map[k]["output_directory_path"] = directory_path
                    export_map[k]["output_file_path"] = os.path.join(directory_path, file_name)
        return export_map

    @staticmethod
    def _get_ff_range(ff0, ff1):
        try:
            ff0 = int(ff0)
            ff1 = int(ff1)
            if ff0 > ff1:
                return range(ff1, ff0 + 1)
            elif ff0 < ff1:
                return range(ff0, ff1 + 1)
            elif ff1 == ff0:
                return [ff0]
            else:
                return False
        except ValueError:
            return False

    def do_export(self):
        tif8 = None
        for k in sorted(self.export_map):
            source_file = self.export_map[k]["file_name"]
            if not os.path.isdir(self.export_map[k]["output_directory_path"]):
                os.makedirs(self.export_map[k]["output_directory_path"])
            # except
            if os.path.exists(self.export_map[k]["output_file_path"]):
                print "File: %s - already exists" % self.export_map[k]["output_file_path"]
                continue
            if self.export_map[k]["format"] == 'tif8':
                if not tif8 or tif8.filename != source_file:
                    if tif8:
                        tif8.close()
                    tif8 = TIF8(source_file, dictionary_input=True, dictionary_output=True, progressBar=False)
                # print "Copy %s - FF: %i  --> %s" % (self.export_map[k]["file_name"], self.export_map[k]["ffid"],
                #                                     self.export_map[k]["output_file_path"])
                tif8.extract_segd_file_by_ff(self.export_map[k]["ffid"],
                                             dest_path=self.export_map[k]["output_file_path"])
                if os.path.getsize(self.export_map[k]["output_file_path"]) != self.export_map[k]["size"]:
                    print "File %s - File size not expected"
            elif self.export_map[k]["format"] == 'sgy':
                # print "Copy %s  --> %s" % (self.export_map[k]["file_name"], self.export_map[k]["output_file_path"])
                shutil.copy(self.export_map[k]["file_name"], self.export_map[k]["output_file_path"])
        return

    def info_tif8(self):
        #     tif8_set = set()
        #     with open(list_input) as fin:
        #         for line in fin:
        #             line = line.split(',')
        #             file_name = line[0]
        #             file_name = file_name.replace('/seismic/seismic/', '/data/seismic/')
        #             if file_name.lower().endswith('.tif8'):
        #                 tif8_set.add(file_name)
        #     for f in tif8_set:
        #         tif8 = TIF8(f, dictionary_input=False, dictionary_output=False, progressBar=False)
        #         tif8.info_tif8()
        #         tif8.info_segd()
        return

    def info_request(self):
        qty_line = len(self.line_list)
        segy_files_qty = self._get_qty_files("sgy")
        tif8_files_qty = self._get_qty_files("tif8")
        segd_files_qty = self._get_qty_files("segd")
        print "Request Name: %s" % self.request_name
        print "Output Directory: %s" % self.output_directory
        print "#Lines: %i" % qty_line
        print "#SEG-Y Files: %i" % segy_files_qty
        print "Size SEG-Y Files: %s" % naturalsize(self._get_export_size_format('sgy'))
        print "#TIF8 Files: %i" % tif8_files_qty
        print "#SEG-D Files: %i" % segd_files_qty
        print "Size SEG-D Files: %s" % naturalsize(self._get_export_size_format('segd'))
        print "Size total: %s" % naturalsize(self.size)
        return

    def info_segd(self):
        return

    def info_segy(self):
        return


if __name__ == '__main__':
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-l", "--list_input", dest="list_input", help="Input list (csv) format")
    parser.add_option("-o", "--output_directory", dest="output_directory", help="Output Directory")
    parser.add_option("-r", "--request_name", dest="request_name", help="Request Name (Name of the parent directory")
    parser.add_option("-t", "--tif8_info", dest="tif8_info", help="Get TIF8 basic info")
    parser.add_option("-d", "--segd_info", dest="segd_info", help="Get SEG-D basic info")
    parser.add_option("-y", "--segy_info", dest="segy_info", help="Get SEG-Y basic info")
    parser.add_option("-i", "--request_info", dest="request_info", help="Get info about the request (#files, size)")
    parser.add_option("-e", "--export", dest="do_export", help="Do export")
    options, args = parser.parse_args()

    list_input = options.list_input
    output_directory = options.output_directory
    request_name = options.request_name
    request_info = options.request_info
    tif8_info = options.tif8_info
    segd_info = options.segd_info
    do_export = options.do_export

    if not list_input:
        print "Input file needed"
        quit()
    if not os.path.exists(list_input):
        print "Input file: %s, doesn't exist" % list_input
        quit()
    if not output_directory:
        print "Output directory needed"
        quit()
    if not os.path.isdir(output_directory):
        print "Output directory: %s, doesn't exist" % output_directory
        quit()
    if not request_name:
        print "Request Name needed"
        quit()

    export_request = ExportSeismic(list_input, output_directory, request_name)

    if request_info:
        export_request.info_request()
    elif tif8_info:
        export_request.info_tif8()
    elif segd_info:
        export_request.info_segd()

    if do_export:
        export_request.do_export()
