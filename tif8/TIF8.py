from struct import unpack
import os
from segd.segd import Segd
from progressbar import ProgressBar, Bar, Percentage, ETA
import cPickle as pickle
from segd.segd_to_segy import ebcdic, binary, EBCDIC_HEADER_LENGTH
from utils import walk_files
import inspect
import datetime

# import sys
# from  progress.spinner import Spinner

TIF8_BLOCK_SIZE = 24
EXT_SEGD = '.segd'
EXT_SEGY = '.sgy'


# TODO: add docstring
# TODO: unittest
# TODO: handling exceptions, try/except, errors
# TODO: logging
# TODO: command line

class Tif8FormatError(Exception):
    pass


class TIF8(Segd):
    def __init__(self, filename, dictionary_output=True, dictionary_input=True, progressBar=True, log=True):
        self.filename = filename
        self.progressBar = progressBar
        self.size = os.path.getsize(filename)
        self.dictionary_output = dictionary_output
        self.dictionary_input = dictionary_input
        self.open_tif8 = self.open(filename)
        self.map_tif8 = self._set_map()
        self.nb_blocks = self._get_nb_blocks()
        self.map_segd = self.inspect_segd()
        self.log = log
        # self.format_files = self._get_file_format()

    def _get_nb_blocks(self):
        """
        Returns the number of blocks of the TIF8 file

        Returns:
            int: number of blocks
        """
        return len(self.map_tif8)

    @staticmethod
    def _get_file_format():
        return

    def _get_pickle_name(self, map_type):
        """
        From the type of map to establish (tif8 or segd) returns the hypothetical pickle filename associated to the
        TIF8 file and map

        Args:
            map_type (str): Type of map we want to create/retrieve. 'segd' or 'tif8'
        """
        # file_name = os.path.basename(self.filename).split('.')[0] + '_map_%s.p' % map_type
        file_name = self.filename.replace('/', '_') + '.map_%s.p' % map_type
        try:
            # os.path.realpath(__file__)
            path = os.path.join(os.path.dirname(__file__), 'dictionaries')
            return os.path.join(path, file_name)
        except:
            return ''

    def close(self):
        if self.open_tif8.closed:
            return None
        else:
            self.open_tif8.close()
            return None

    def _set_map(self):
        """
        Reads all the TIF8 blocks by induction to establish a map dictionary of the TIF8 file

        offset0: Offset of the previous block
        offset1: Offset of the next block
        offset: Offset of the current block
        offset_data: Offset of the data in the current block
        size_data: Size of the data in the current block

        Returns:
            dict: Map of the TIF8 file
            keys level0 = block_nb
            values level0 = dictionary containing block information
            keys level1 = offset0, offset1, offset_data, offset, record_type, size_data
        """
        pickle_file = self._get_pickle_name('tif8')
        if os.path.isfile(pickle_file) and self.dictionary_input:
            return pickle.load(open(pickle_file, "rb"))
        map_tif8 = {}
        offset1 = 0
        block = 1
        size_scanned = 0
        if self.progressBar:
            pbar = ProgressBar(maxval=self.size,
                               widgets=['Index TIF8 file ', Bar('#', '[', ']'), ' ', Percentage(), ' ', ETA()])
            pbar.start()
        while True:
            self.open_tif8.seek(offset1)
            tif8_block = self.open_tif8.read(TIF8_BLOCK_SIZE)
            if not tif8_block:
                break
            map_tif8[block] = {"offset": offset1,
                               "record_type": unpack('<i', tif8_block[4:8])[0],
                               "offset_data": offset1 + TIF8_BLOCK_SIZE
                               }
            offset0 = unpack('<q', tif8_block[8:16])[0]
            if block != 1 and offset0 != map_tif8[block - 1]["offset"]:
                print "File TIF8 %s corrupted - block %i\n" % (self.filename, block)
            offset1 = unpack('<q', tif8_block[16:24])[0]
            map_tif8[block]["offset0"] = offset0
            map_tif8[block]["offset1"] = offset1
            map_tif8[block]["size_data"] = offset1 - map_tif8[block]["offset_data"]
            size_scanned += TIF8_BLOCK_SIZE + map_tif8[block]["size_data"]
            if self.progressBar:
                pbar.update(size_scanned)
            if map_tif8[block]["offset_data"] >= self.size or map_tif8[block]["offset1"] >= self.size:
                break
            block += 1
        if self.progressBar:
            pbar.finish()
        if self.dictionary_output:
            pickle.dump(map_tif8, open(pickle_file, "wb"))
        return map_tif8

    def info_tif8(self):
        """Print common info of the TIF8 object"""
        buf = "\nCommon Information\n\n"
        buf += "File Name            : %s\n" % self.filename
        buf += "File Size (bytes)    : %s\n" % self.size
        buf += "Number of blocks     : %s\n\n" % self.nb_blocks
        print buf

    def info_tif8_full(self):
        """Print Full info of the TIF8 object"""
        buf = "\nCommon Information\n\n"
        buf += "File Name            : %s\n" % self.filename
        buf += "File Size (bytes)    : %s\n" % self.size
        buf += "Number of blocks     : %s\n\n" % self.nb_blocks
        buf += "Blocks Description   :\n"
        for k in self.map_tif8.iterkeys():
            buf += "Block #: %-7i - Block Offset: %-9i - Block Size: %-9i\n" % (k, self.map_tif8[k][
                'offset'],
                                                                         self.map_tif8[k][ 'size_data'])
        print buf

    def info_tif8_csv(self, header=True, stdout=True):
        """Print TIF8 object details in csv format"""
        buf = ""
        if header:
            buf +=  "file_name,block_number,offset,size,record_type\n"
        for k in self.map_tif8.iterkeys():
            buf += "%s,%i,%i,%i,%i,\n" % (self.filename, k, self.map_tif8[k]['offset'], self.map_tif8[k]['size_data'],
                                      self.map_tif8[k]['record_type'])
        if stdout:
            print buf
        return buf

    def info_segd_full(self):
        """Print Full info of the SEG-D files encapsulated"""
        buf = "\nSEG-D Information\n\n"
        buf += "#SEG-D files encapsulated : %i\n" % len(self.map_segd)
        if len(self.map_segd) > 0:
            buf += "FF first SEG-D file       : %s\n" % self.map_segd[1]["ff"]
            buf += "FF last  SEG-D file       : %s\n\n" % self.map_segd[len(self.map_segd)]["ff"]
        for k in self.map_segd.iterkeys():
            try:
                buf += "FFS #: %-4i - Offset: %-9i - FF: %-6i - SP: -%4i - #Channels: %-5i - Sample Rate: %-1.1f ms - " \
                       "Record Length: %-5i ms - Size: %-8i\n" % (k, self.map_segd[k]["offset"], self.map_segd[k]["ff"],
                    self.map_segd[k]["sp"],
                    self.map_segd[k]["nb_traces"], self.map_segd[k]["sample_rate"],
                    self.map_segd[k]["record_length"],
                    self.map_segd[k]["size"])
            except KeyError:
                continue
        print buf

    def info_segd(self):
        """Print common info of SEG-D files encapsulated"""
        buf = "\nSEG-D Information\n\n"
        buf += "#SEG-D files encapsulated : %i\n" % len(self.map_segd)
        if len(self.map_segd) > 0:
            buf += "FF first SEG-D file       : %s\n" % self.map_segd[1]["ff"]
            buf += "FF last  SEG-D file       : %s\n" % self.map_segd[len(self.map_segd)]["ff"]
        print buf

    def info_segd_csv(self, header=True, stdout=True):
        """Print SEG-D object details in csv format"""
        buf = ""
        if header:
            buf += "file_name,ffs,offset,ff,sp,nb_traces,sample_rate,record_length,size,status,\n"
        for k in self.map_segd.iterkeys():
            try:
                buf += "%s,%i,%i,%i,%i,%i,%i,%i,%i,%i,\n" % (
                    self.filename, k, self.map_segd[k]["offset"], self.map_segd[k]["ff"],
                    self.map_segd[k]["sp"],
                    self.map_segd[k]["nb_traces"], self.map_segd[k]["sample_rate"],
                    self.map_segd[k]["record_length"],
                    self.map_segd[k]["size"],
                    self.map_segd[k]["status"])
            except KeyError:
                continue
        if stdout:
            print buf
        return buf

    def inspect_segd(self, block_eof=1):
        """Creates map_segd dictionary. For each SEG-D a SEG-D object is created from the 2 first traces to get the
        SEG-D info (2 traces to test the sercel byte issue)"""
        pickle_file = self._get_pickle_name('segd')
        if os.path.isfile(pickle_file) and self.dictionary_input:
            return pickle.load(open(pickle_file, "rb"))
        map_segd = {}
        block = 1
        segd_nb = 0
        if self.progressBar:
            pbar = ProgressBar(maxval=self.nb_blocks,
                               widgets=['Index SEGD files ',
                                        Bar('#', '[', ']'), ' ',
                                        Percentage(), ' ',
                                        ETA()])
            pbar.start()
        while True and block != self.nb_blocks:
            # read block header
            block_header_2tr = self.read_block(block)
            # read first trace
            block_header_2tr += self.read_block(block + 1)
            # read 2nd trace
            block_header_2tr += self.read_block(block + 2)

            segd_header_2tr = Segd(bytestream=block_header_2tr, container=self.filename, ffs=segd_nb + 1,
                                   inspect_nb_samples=False)

            # TODO: Try to reach next file with empty TIF8 blocks
            if not hasattr(segd_header_2tr, 'ff'):  # Case not SEG-D file. Unable to seek next file. Stop SEG-D analysis
                print "TIF8 %s - file #%i not structured as SEG-D" % (self.filename, segd_nb + 1)
                if self.dictionary_output:
                    pickle.dump(map_segd, open(pickle_file, "wb"))
                return map_segd

            segd_nb += 1

            # SEG-D map dictionary: Most of keys are info relative to TIF8
            # (see notes to understand why Segd.__dict__ not copied)
            map_segd[segd_nb] = {}
            map_segd[segd_nb]["offset"] = self.map_tif8[block]["offset"]
            map_segd[segd_nb]["ff"] = segd_header_2tr.ff
            if segd_header_2tr.sp:
                map_segd[segd_nb]["sp"] = segd_header_2tr.sp
            else:
                map_segd[segd_nb]["sp"] = 0
            map_segd[segd_nb]["sample_rate"] = segd_header_2tr.sample_rate
            map_segd[segd_nb]["record_length"] = segd_header_2tr.record_length
            map_segd[segd_nb]["start_block"] = block
            map_segd[segd_nb]["nb_traces"] = segd_header_2tr.total_traces
            map_segd[segd_nb]["nb_aux_traces"] = segd_header_2tr.get_aux_traces()
            map_segd[segd_nb]["sample_pattern"] = segd_header_2tr.get_nb_samples_pattern()
            size_segd = self.map_tif8[block]["size_data"]
            for i in range(1, map_segd[segd_nb]["nb_traces"] + 1):
                try:
                    size_segd += self.map_tif8[block + i]["size_data"]
                # Handle case where number of blocks of TIF8 is lower than number of blocks specified by the SEG-D file
                except KeyError:
                    print "TIF8 %s - SEG-D #%i truncated" % (self.filename, segd_nb)
                    if self.dictionary_output:
                        pickle.dump(map_segd, open(pickle_file, "wb"))
                    map_segd[segd_nb]["status"] = 0
                    map_segd[segd_nb]["size"] = size_segd
                    return map_segd
            map_segd[segd_nb]["status"] = 1
            map_segd[segd_nb]["size"] = size_segd

            if self.progressBar:
                pbar.update(block)

            # We skip the cursor for the next iteration to the block where the next SEG-D file starts
            block += map_segd[segd_nb]["nb_traces"] + 1 + block_eof
            # To handle SEG-D files separated by n number of empty blocks
            for i in [block, self.nb_blocks]:
                if self.map_tif8[i]["size_data"] == 0:
                    block += 1
                else:
                    break

        if self.progressBar:
            pbar.finish()
        if self.dictionary_output:
            pickle.dump(map_segd, open(pickle_file, "wb"))
        return map_segd

    def _get_output_file_name(self, ffs, file_name_ffs=False, dest_folder='./', dest_path=None, ext=EXT_SEGD):
        """Called by extract_segd_file to set the output file name. Can be set in function of the sequential SEG-D nb
        into the TIF8 or in function of the FF nb. In both case ffs or ff nb are filled with leading 0 to get a
        unique filename length into the TIF8 ensemble

        Args:
            file_name_ffs (bool): filename defined by SEG-D sequential nb (True) or Field File nb (False)
            dest_folder (str): Output path
            ffs (int): Sequential nb of the segd to get the info (ffs or ff) for the self.map_segd dict

        Returns:
            str: File name
        """
        if dest_path:
            return dest_path
        if file_name_ffs:
            lead0_ffs = len(str(len(self.map_segd)))
            return os.path.join(dest_folder, str(ffs).zfill(lead0_ffs) + ext)
        else:
            lead0_ff = len(str(max(segd_file["ff"] for segd_file in self.map_segd.itervalues())))
            return os.path.join(dest_folder, str(self.map_segd[ffs]["ff"]).zfill(lead0_ff) + ext)

    def read_block(self, block_nb, output_file=None):
        """
        Read TIF8 block (data no TIF8 header) and returns a bytestream or a output file

        Args:
            block_nb (int): Sequential nb of the TIF8 block to read. Used to extract the information from self.map_tif8
            output_file (str): If not set the method returns a bytestream of the TIF8 block

        Returns:
            str: Bytestream in case output_file == None
        """
        block = str()
        self.open_tif8.seek(self.map_tif8[block_nb]["offset_data"])
        block += self.open_tif8.read(self.map_tif8[block_nb]["size_data"])

        if output_file:
            with open(output_file, 'a+') as fout:
                fout.write(block)
                print "Extract block# %i from %s --> %s" % (block_nb, os.path.basename(self.filename), output_file)
        else:
            return block

    def extract_segd_file(self, all_files=False, filename_ffs=False, dest_folder='./', dest_path=None, bytestream=False,
                          list_segd_nb=None, force_error=False):
        """
        Method that returns SEG-D files extracted from the TIF8 file.
        Can return one or multiple files in a bytestream (only when extracts one file) or in output file

        Args:
            all_files (bool): True implies to convert all the SEG-D files encapsulated in the TIF8 object
            (indexed in self.map_segd). If False a list of SEG-D files nb is required

            list_segd_nb (iterable): List of segd file nb (Sequential number into the TIF8 file) to extract

            bytestream (bool): If True the method returns a bytestream of the SEG-D file to be extracted. It implies
            to extract only one SEG-D file (len(list_Segd_nb) == 1).
            If False the extracted file(s) with be copied to individual SEG-D files

            filename_ffs (bool): In case of bytestream == True, sets the name of the output filename. If True the output
            file name will reflect the sequential nb of the SEG-D file, else it will reflect the Field File nb

            dest_folder (str): The output folder where to extract the SEG-D file (if bytestream == False)

        Returns:
            str: Byte stream in case one file to extract and bytestream == True
            tuple: Tuple that contains the name of the output files (with bytestream == False)

        """
        if all_files:
            list_segd_nb = [k for k in self.map_segd.iterkeys()]
        elif not all_files and not list_segd_nb:
            print "Error (extract_segd_file): An iterable with the list of SEG-D files nb is required"
            return
        list_out = ()
        for ffs in list_segd_nb:
            if ffs not in self.map_segd.iterkeys():
                print "ffs #%i not in TIF8 file" % ffs
                continue
            segd_file = str()
            segd_file += self.read_block(self.map_segd[ffs]["start_block"])
            for tr_nb in range(1, self.map_segd[ffs]["nb_traces"] + 1):
                try:
                    segd_file += self.read_block(self.map_segd[ffs]["start_block"] + tr_nb)
                except KeyError:
                    break
            if not force_error:
                segd_file_qc = Segd(segd_file).qc()
                if not segd_file_qc:
                    print "SEG-D file - ffs #%i corrupted. Output skipped" % ffs
                    continue
            if bytestream:
                return segd_file
            else:
                out = self._get_output_file_name(ffs, filename_ffs, dest_folder, dest_path)
                list_out += (out,)
                with open(out, 'a+') as fout:
                    fout.write(segd_file)
                    print "Extract SEG-D file  from %s - ffs #%i --> %s" % (os.path.basename(self.filename), ffs, out)
        return list_out

    def extract_segd_file_by_ff(self, ff=None, filename_ffs=False, dest_folder='./', dest_path=None,
                                bytestream=False, force_error=False):
        """
        Method that returns one SEG-D file extracted from the TIF8 file according to its FF number.
        Can return the file in a bytestream or in output file

        Args:
            ff: Field File Number to extract

            bytestream (bool): If True the method returns a bytestream of the SEG-D file to be extracted.
            If False the extracted file(s) with be copied to individual SEG-D files

            filename_ffs (bool): In case of bytestream == True, sets the name of the output filename. If True the output
            file name will reflect the sequential nb of the SEG-D file, else it will reflect the Field File nb

            dest_folder (str): The output folder where to extract the SEG-D file (if bytestream == False)

        Returns:
            str: Byte stream in case one file to extract and bytestream == True
            tuple: Tuple that contains the name of the output files (with bytestream == False)
        """
        ffs = self.search_ff(ff)
        if ffs > 0:
            return self.extract_segd_file(filename_ffs=filename_ffs, dest_folder=dest_folder, dest_path=dest_path,
                                          list_segd_nb=[ffs], bytestream=bytestream, force_error=force_error)
        elif ffs == 0:
            print "No Field File Number %i were found in %s" % (ff, os.path.basename(self.filename))
        elif ffs == -1:
            print "Multiple Field File Number %i were found in %s" % (ff, os.path.basename(self.filename))

    def search_ff(self, ff):
        """
        Method that returns the ffs of a given FF number into the TIF8.

        Args:
            ff (int): Field File Number

        Returns:
            ffs: File Sequential number into the TIF8.
            Returns 0 if FF not found
            Returns -1 if multiple files are found
        """
        ffs = 0
        for k in self.map_segd.iterkeys():
            if self.map_segd[k]["ff"] == ff and ffs == 0:
                ffs = k
            elif self.map_segd[k]["ff"] == ff and ffs != 0:
                ffs = -1
                return ffs
        return ffs

    def size_segd(self, ff):
        """
        Method that returns the size of an encapsulated SEG-D

        Args:
            ff (int): Field File Number

        Returns:
            Returns -1 if FF not found
            Returns -2 if multiple files are found
        """
        search = self.search_ff(ff)
        if search == 0:
            return -1
        if search == -1:
            return -2
        else:
            return self.map_segd[search]["size"]

    def convert_tif8_to_segy(self, split=True, output_file=None, string_ebcdic=None, ascii_textual=False,
                             file_ebcdic=None, format_code=5, resample=None, big_endian=True, sorting_code=1,
                             segy_rev=1, extended_textual_header=0, binary_header_dict=None, limit_tr_0=1,
                             limit_tr_1_ref=None, list_segd_file=None, force_sercel_byte=False, verbose=False,
                             progressBar=False, file_name_ffs=False, output_folder=None):

        if not list_segd_file:
            list_segd_file = [k for k in self.map_segd.iterkeys()]
        if split:
            for i in list_segd_file:
                segd_str = Segd(bytestream=self.extract_segd_file(bytestream=True, list_segd_nb=(i,)), ffs=i,
                                container=True, force_sercel_byte=force_sercel_byte, verbose=verbose)
                segd_str.convert_to_segy(output_file=self._get_output_file_name(i, ext=EXT_SEGY,
                                                                                file_name_ffs=file_name_ffs,
                                                                                dest_folder=output_folder),
                                         string_ebcdic=string_ebcdic, ascii_textual=ascii_textual,
                                         file_ebcdic=file_ebcdic, format_code=format_code, resample=resample,
                                         big_endian=big_endian, sorting_code=sorting_code, segy_rev=segy_rev,
                                         extended_textual_header=extended_textual_header,
                                         binary_header_dict=binary_header_dict, limit0=limit_tr_0, limit1=limit_tr_1,
                                         container=True)
        else:
            if not output_file:  # If not output file_name precised for convert whole tif8 (split=False) the output
                # file name is the TIF8 file name changing the extension to sgy
                output_file = self.filename.split('.')[0] + EXT_SEGY
            with open(output_file, 'wb+') as segy_file:
                segy_file.write(ebcdic(string=string_ebcdic, file_input=file_ebcdic, ascii=ascii_textual))
                segy_file.write(400 * ' ')

                aux_traces = 0
                tr_nb_within_file = 1
                sample_pattern_ref = None
                sample_rate = 0
                pbar = ProgressBar(maxval=list_segd_file[-1],
                                   widgets=['Merge SEG-D files ', Bar('#', '[', ']'), ' ', Percentage(), ' ', ETA()])
                pbar.start()
                for i in list_segd_file:
                    segd_file = Segd(bytestream=self.extract_segd_file(bytestream=True, list_segd_nb=(i,)), ffs=1,
                                     container=True, force_sercel_byte=force_sercel_byte, verbose=verbose)
                    segd_qc = segd_file.qc()
                    if not segd_qc:
                        print "%s - ffs %i: SEG-D corrupted skipped" % (str(datetime.datetime.now()).split('.')[0], i)
                        continue
                    aux_traces += segd_file.get_aux_traces()
                    sample_pattern = segd_file.get_nb_samples_pattern()
                    if not sample_pattern_ref:
                        sample_pattern_ref = sample_pattern
                        sample_rate = segd_file.sample_rate
                    elif sample_pattern[1] != sample_pattern_ref[1] or sample_pattern[0] != sample_pattern_ref[0]:
                        sample_pattern_ref[1] = 0

                    if not limit_tr_1_ref:
                        limit_tr_1 = segd_file.total_traces

                    segd_file.write_segy_traces(segy_file,
                                                big_endian=big_endian,
                                                resample=resample,
                                                trace_sorting_code=sorting_code,
                                                format=format_code,
                                                limit0=limit_tr_0,
                                                limit1=limit_tr_1,
                                                first_trace_nb=tr_nb_within_file)
                    print "%s - ffs %i - FFID %i: Appened to %s" % (str(datetime.datetime.now()).split('.')[0], i,
                                                              segd_file.ff, output_file)

                    if not limit_tr_1_ref:
                        limit_tr_1 = segd_file.total_traces
                    tr_nb_within_file += limit_tr_1 - limit_tr_0 + 1
                    if progressBar:
                        pbar.update(i)

                segy_file.seek(EBCDIC_HEADER_LENGTH)
                segy_file.write(binary(create_binary_dict(format=format_code,
                                                          trace_sorting_code=sorting_code,
                                                          segy_rev=segy_rev,
                                                          extended_textual_header=extended_textual_header,
                                                          binary_header_dict=binary_header_dict,
                                                          aux_tr=aux_traces,
                                                          total_traces=tr_nb_within_file - 1,
                                                          nb_samples_pattern=sample_pattern_ref,
                                                          sample_rate=sample_rate)))

                if progressBar:
                    pbar.finish()
                print "File %s created" % output_file


def create_binary_dict(format=5, trace_sorting_code=1, segy_rev=1, extended_textual_header=0,
                       binary_header_dict=None, aux_tr=None, total_traces=None, nb_samples_pattern=None,
                       sample_rate=None):
    """
        Create a 'minimum' binary dictionary composed by the value of the mandatory fields of
        the binary header read from the object's attributes or from the convert_to_segy method's arguments.
        Possibility to overwrite 'structural' values with the binary_header_dict passed as arguments

        Returns:
            dict:
            key = Name of the header (This name must be part of the headers set in the binary_header dictionary
            (see constants_sgy.py)
            value = raw value (will be packed in binary function)
        """
    binary_dict = dict()
    if binary_header_dict:
        for header, value in binary_header_dict.iteritems():
            binary_dict[header] = value
    if "traces_per_ensemble" not in binary_dict:
        binary_dict["traces_per_ensemble"] = total_traces
        # binary_dict["traces_per_ensemble"] = 65535
    if "aux_traces_per_ensemble" not in binary_dict:
        binary_dict["aux_traces_per_ensemble"] = aux_tr
    if "nb_samples_per_trace" not in binary_dict:
        binary_dict["nb_samples_per_trace"] = nb_samples_pattern[0]
    if "fixed_length_flag" not in binary_dict:
        binary_dict["fixed_length_flag"] = nb_samples_pattern[1]
    if "sample_interval" not in binary_dict:
        binary_dict["sample_interval"] = sample_rate * 1000
    if "format_code_segy" not in binary_dict:
        binary_dict["format_code_segy"] = format
    if "trace_sorting" not in binary_dict:
        binary_dict["trace_sorting"] = trace_sorting_code
    if "segy_rev" not in binary_dict:
        binary_dict["segy_rev"] = segy_rev
    if "extended_textual_header" not in binary_dict:
        binary_dict["extended_textual_header"] = extended_textual_header
    return binary_dict


if __name__ == '__main__':
    # from optparse import OptionParser
    #
    # parser = OptionParser()
    # parser.add_option("-f", "--file", dest="tif8_file", help="TIF8 file path", default='')
    # parser.add_option("-d", "--folder", dest="tif8_folder", help="TIF8 folder path", default='')
    # parser.add_option("--info", dest="info", help="Get TIF8 info", default=False)
    # parser.add_option("--segd_info", dest="segd_info", help="Get SEG-D files info", default=False)
    # parser.add_option("--segd_dump", dest="segd_dump", help="Get SEG-D files dump", default=False)
    # options, args = parser.parse_args()
    #
    # if options.tif8_folder:
    #     for f in walk_files(options.tif8_folder):
    #         if f.lower().endswith('.tif8'):
    #             print f
    #             tif8 = TIF8(f, dictionary_output=False, dictionary_input=False, progressBar=False)
    #             if options.info:
    #                 tif8.info_tif8()
    #
    #             if options.segd_info:
    #                 tif8.info_segd()

    #             if options.segd_dump:
    #                 tif8.info_segd_dump()
    # elif options.tif8_file:
    #     tif8_path = options.tif8_file
    #     tif8 = TIF8(tif8_path, dictionary_output=False, dictionary_input=False, progressBar=False)
    #     if options.info:
    #         tif8.info_tif8()
    #
    #     if options.segd_info:
    #         tif8.info_segd()
    #
    #     if options.segd_dump:
    #         tif8.info_segd_dump()
    # else:
    #     print "Needs file path or folder in input"

    # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_96X02___1513.tif8')
    # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_96X02___0021.tif8')
    # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_96X02___0021.tif8')
    # a = TIF8('/home/thibault/Desktop/projects/ogdr/migration/seis_migration/FLD_99SDO___7833.tif8',
    #          dictionary_input=False)
    # # a.info_segd_dump()
    # a.extract_segd_file(
    #     dest_folder="/home/thibault/Desktop/projects/ogdr/migration/seis_migration/",
    #     list_segd_nb=range(30, 35))

    # # TODO: No SEG-D file - To review
    # # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_99SDO___8307.tif8')
    #
    # # TODO: 8 bytes of unknown data at the end of each trace - TIF8 structure/mapping issue? - SEG-D issue? - To review
    # # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_91LW____8800.tif8',
    # #          dictionary_input=False, dictionary_output=False)
    #
    # # TODO:  3 first blocks of ffs 50 correspond to header block only -  To review - blocking
    # # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_91LW____8946.tif8',
    # #          dictionary_input=False, dictionary_output=False)
    #
    # # TODO:  To review - blocking
    # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_96SU____1108.tif8',
    #          dictionary_input=False, dictionary_output=False)
    #
    # # TODO:  To review - Block 48 blocking
    # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_94HD____0398.tif8',
    #          dictionary_input=False, dictionary_output=False)

    # a = TIF8('/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/FLD_99SDO___7833.tif8')


    # a.convert_tif8_to_segy(list_segd_file=(65, 68))
    # a.convert_tif8_to_segy(split=False, list_segd_file=range(1, 1000), limit_tr_0=75, limit_tr_1_ref=75)
    # a = TIF8('/run/user/1000/gvfs/smb-share:server=10.10.10.60,sharlimit_tr_0=75, limit_tr_1_ref=75)
    # fs/tibo/2D_FIELDSEGD/99SDO0101A.TIF8')
    # a = TIF8('/home/thibaultuzu/Desktop/tibo_ifs/2D_FIELDSEGD/99SDO0101A.TIF8')
    # a.extract_segd_file(dest_folder="/home/thibaultuzu/Desktop/samples/samples_OGDR/seismic/3D_FIELDSEGD/extract/FLD_99SDO___8535",
    #                     list_segd_nb=range(1, 10))
    # a.info_tif8()
    # a.info_segd()
    # a.info_segd_dump()
    # a.extract_segd_file(all_files=True, filename_ffs=False,
    #                     dest_folder='/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/segd_export/FLD_99SDO___8535',
    #                     bytestream=False)
    # a.extract_segd_file_by_ff(ff=91, filename_ffs=False,
    #                           dest_folder='/home/thibault/Desktop/projects/ogdr/operator_requests/20160111_oocep/segd_export/')
    # a.extract_segd_file(dest_folder="/home/thibaultuzu/Desktop/samples/samples_OGDR/seismic/3D_FIELDSEGD/extract",
    #                     list_segd_nb=[215, 216, 217, 218])
    # for ffs in a.map_segd.iterkeys():
    #     # for ffs in [217, 218]:
    #     #     print ffs
    #     segd = Segd(bytestream=a.extract_segd_file(bytestream=True, list_segd_nb=[ffs]))
    #     segd.info()
    #     # segd.info_gh1()
    #     # segd.info_gh2()
    #     # segd.info_ghn()
    #     # segd.dump_block_header("extended_header", length=32)
    #     segd.dump_block_header("external_header")
    #
    #     # segd.add_header(name="survey", block_header="external_header", rel_offset=0, type='s16', strip=False)
    #     # segd.add_header(name="reel", block_header="extended_header", rel_offset=2060, type='s6', strip=False)
    #
    #     # segd.add_header(name="sp", block_header="ghn", block_number=segd.general_header_blocks,
    #     #                 rel_offset=8, type='N3')
    #     # segd.add_header(name="line", block_header="ghn", block_number=segd.general_header_blocks,
    #     #                 rel_offset=3, type='N3')
    #
    #     # sys.stdout.write("%s," % a.filename)
    #     # sys.stdout.flush()
    #     # segd.export_csv("ff", "sp", "line", "survey")
    #     # segd.export_csv("ff", "reel")
    #
    #     # segd.dump_block_header("external_header")
    #     # import pickle
    #     # pickle.dump(a.map_tif8, open("Sw059D_2_map_tif8.p", "wb"))
    #     # pickle.dump(a.map_segd, open("Sw059D_2_map_segd.p", "wb"))
    #     # a.extract_segd_file(all_files=False, filename_ffs=False,
    #     #                     dest_folder='/home/thibaultuzu/Desktop/samples/samples_OGDR/seismic/3D_FIELDSEGD/extract',
    #     #                     bytestream=False, list_segd_nb=(78, 45))
    #     # a.extract_segd_file(True, False, '/home/thibaultuzu/Desktop/samples/samples_OGDR/3D_FIELDSEGD/extract/')
    #     # a.convert_tif8_to_segy(list_segd_file=(78, 45))

    a = TIF8('/home/thibault/Desktop/projects/work/FLD_92QB____0526.tif8')
    # a.info_segd()
    # a.info_tif8()
    # a.info_tif8_full()
    a.info_segd_full()
    a.extract_segd_file(all_files=True, dest_folder='/home/thibault/Desktop/projects/work/')
    a.convert_tif8_to_segy(split=False, force_sercel_byte=2, verbose=False)
